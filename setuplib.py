from setuptools import setup, find_packages
from os.path import join, dirname

root = 'tasktracklib'
packages = [root] + [root + '.' + item for item in find_packages(root)]

setup(name='tasktracklib',
      version='1.0',
      description='task tracker',
      packages=packages,
      include_package_data=False,
      install_requires=['enum34==1.1.6', 'mock==2.0.0'])
