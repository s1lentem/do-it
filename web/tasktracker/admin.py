from django.contrib import admin
from .models import (Task, Timing, Comment, Dependence, Status, GroupTasks,
                     ManagerTask, AccessRights, Notification)

admin.site.register(Task)
admin.site.register(Timing)
admin.site.register(Comment)
admin.site.register(Dependence)
admin.site.register(Status)
admin.site.register(GroupTasks)
admin.site.register(ManagerTask)
admin.site.register(AccessRights)
admin.site.register(Notification)
