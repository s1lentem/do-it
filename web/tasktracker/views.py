from django import forms
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.views.generic.base import View
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from .models import (Task, Timing, Status, Dependence, GroupTasks, Comment,
                     Notification)
from .models import ManagerTask as Mt
from .forms import (GroupTasksForm, CreateTaskForm, CreateTaskManagerForm,
                    GiveAccessRightForm, FilterTasksForm, LinkForm)

from tasktracklib.storages.storage_interface import (ITaskStorage,
                                                     IUserStorage,
                                                     ITaskManagerStorage)
from tasktracker.ormstorage.ormstorage import (ORMTaskStorage,
                                               ORMTaskManagerStorage,
                                               ORMUserStorage)

from tasktracklib.storagemanager import DataStorageManager
from tasktracklib.components.task import Task as Ts
from tasktracklib.components.task import GroupTasks as Gt
from tasktracklib.components.task import ManagerTask
from tasktracklib.components.taskcomp import Timing as Tm
from tasktracklib.components.taskcomp import Status as St
from tasktracklib.components.taskcomp import Priority
from tasktracklib.components.user import AccessType
from tasktracklib.dateparse import get_datetime
from tasktracklib.errors import EndTaskGeneration, DuplicateTagError, AccessToYourself
from datetime import datetime, timedelta


import datetime
import logging

NOT_IS_AUTHENTICATED_USER_URL = 'tasktracker/main.html'

FORMAT = u'%(levelname)-8s [%(asctime)s]  %(message)s'
logging.basicConfig(filename='request.log', format=FORMAT, level=logging.DEBUG)
LOGGER = logging.getLogger('request')

class RegisterFormView(FormView):
    form_class = UserCreationForm
    success_url = "/login/"
    template_name = "tasktracker/register.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "tasktracker/login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


def post_tasks(request):
    if request.user.is_authenticated():
        user = request.user
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        task = data_storage.task_storage.get_task_by_tag('tag')
        return render(request, 'tasktracker/task.html', {'username': request.user.username,
                                                         'task': task})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})


def search_tasks(request):
    if request.user.is_authenticated():
        username = request.user.username
        if request.method == 'POST':
            LOGGER.info('User searches for tasks')
            form = FilterTasksForm(request.POST)
            if form.is_valid():
                data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                                  ORMUserStorage(request.user),
                                                  ORMTaskManagerStorage(request.user),
                                                  username)
                if form.cleaned_data['search_scope'] == 'archiv':
                    group = data_storage.get_tasks('arch')
                    tasks = list() if group is None else group.tasks
                elif form.cleaned_data['search_scope'] == 'not_archiv':
                    tasks = data_storage.get_tasks()
                else:
                    group = data_storage.get_tasks('arch')
                    temp = list() if group is None else group.tasks
                    tasks = data_storage.get_tasks() + temp

                if form.cleaned_data['tag'] != '':
                    tasks = [task for task in tasks if form.cleaned_data['tag'] in task.tag]

                if form.cleaned_data['description'] != '':
                    tasks = [task for task in tasks if form.cleaned_data['description'] in task.description]

                if form.cleaned_data['description'] != '':
                    tasks = [task for task in tasks if form.cleaned_data['description'] in task.description]

                if form.cleaned_data['priority'] != 'none':
                    tasks = [task for task in tasks if form.cleaned_data['priority'] == task.status.priority.name]

                if form.cleaned_data['status'] != 'none':
                    tasks = [task for task in tasks if form.cleaned_data['status'] == task.status.state.name]

                if form.cleaned_data['deadline'] is not None:
                    deadline = datetime.datetime(form.cleaned_data['deadline'].year,
                                                 form.cleaned_data['deadline'].month,
                                                 form.cleaned_data['deadline'].day,
                                                 form.cleaned_data['deadline'].hour,
                                                 form.cleaned_data['deadline'].minute)
                    tasks = [task for task in tasks if deadline > task.timing.end]

                LOGGER.info('The user has found {0} tasks'.format(len(tasks)))
                for task in tasks:
                    task.link_tag = task.tag.replace(' ','_') +  ';' + username
                return render(request, 'tasktracker/search.html', {'username': username,
                                                                   'form': form,
                                                                   'tasks': tasks})
        form = FilterTasksForm()
        return render(request, 'tasktracker/search.html', {'username': username,
                                                           'form': form})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})


def post_user_info(request):
    if request.user.is_authenticated():
        LOGGER.info('The user {0} displays information about himself'.format(request.user.username))
        user = request.user
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        count_task = len(data_storage.get_tasks())

        if request.method == 'POST':
            if 'check_tasks' in request.POST or 'notifycation' in request.POST:
                try:
                    data_storage.check_tasks_from_managers()
                except EndTaskGeneration as e:
                    message = 'The "{0}" dates have expired to generate tasks'.format(e.manager.tag)
                    data_storage.send_notification([request.user.username],
                                                   message)
                    data_storage.user_storage.save_state()

                failed_tasks = data_storage.check_deadline_for_each_task()
                for task in failed_tasks:
                    data_storage.send_notification([user.username],
                                                   'Task ' + task.tag + ' is failed')
                    data_storage.user_storage.save_state()

            if 'notifycation' in request.POST:
                LOGGER.info('Notifycations')
                messages = data_storage.get_all_messages_user(user.username, False)
                return redirect('notifications')

        return render(request, 'tasktracker/userinfo.html', {'username': user.username,
                                                             'lent': count_task})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})


def get_messages(request):
    if request.user.is_authenticated():
        user = request.user
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        if request.method == 'POST':
            if 'clear' in request.POST:
                messages = data_storage.get_all_messages_user(user.username)
                return redirect('notifications')
        messages = data_storage.get_all_messages_user(user.username, False)
        return render(request, 'tasktracker/messages.html',
                      {'username': user.username, 'messages': messages})
    return redirect('main')


def post_other_task(request, info):
    if request.user.is_authenticated():
        LOGGER.info('The user {0} looks at the tasks available to him'.format(request.user.username))

        user_info = User.objects.get(username=info.split(';')[0])
        tag = info.split(';')[1]
        data_storage = DataStorageManager(ORMTaskStorage(user_info),
                                          ORMUserStorage(user_info),
                                          ORMTaskManagerStorage(user_info),
                                          user_info.username)
        task = data_storage.get_task_by_tag(tag)
        return render(request, 'tasktracker/available_tasks.html',
                      {'username': user_info.username, 'task': task})
    redirect('main')

def get_boards(request):
    if request.user.is_authenticated():

        LOGGER.info('User {0} view all self boards'.format(request.user.username))

        user = request.user
        groups = GroupTasks.objects.filter(user=request.user)
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        all_groups = list()
        for group_name in [item.name for item in groups]:
            all_groups.append(Gt(group_name, data_storage.get_tasks('group', group_name)))
        for group in all_groups:
            for task in group.tasks:
                task.link_tag = task.tag.replace(' ','_') +  ';' + user.username

        return render(request, 'tasktracker/tasks.html', {'username': user.username,
                                                          'groups': all_groups})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})

def get_archiv(request):
    if request.user.is_authenticated():

        LOGGER.info('User {0} view self archiv'.format(request.user.username))

        data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                          ORMUserStorage(request.user),
                                          ORMTaskManagerStorage(request.user),
                                          request.user.username)
        if 'clear' in request.POST:
            data_storage.reset_archiv()
            archived_tasks = list()
        else:
            archived_tasks = data_storage.get_tasks('arch').tasks
        return render(request, 'tasktracker/archiv.html', {'username': request.user.username,
                                                            'tasks': archived_tasks})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})


def get_access(request):
    if request.user.is_authenticated():
        data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                          ORMUserStorage(request.user),
                                          ORMTaskManagerStorage(request.user),
                                          request.user.username)
        tasks = data_storage.get_tasks_are_available_for()

        LOGGER.info('User {0} get all available tasks'.format(request.user.username))

        return render(request, 'tasktracker/other_tasks.html',
                      {'username': request.user.username, 'tasks': tasks})
    return render(request, NOT_IS_AUTHENTICATED_USER_URL, {})


def get_task_info(request, tag):
    if request.user.is_authenticated():
        user = request.user
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        tag = tag.split(';')[0].replace('_',' ')
        task = data_storage.get_task_by_tag(tag)

        if 'complete_task' in request.POST:
            data_storage.complete_task(tag)
            return redirect('tasks/' + tag + ';' + request.user.username)

        elif 'archieved' in request.POST:
            data_storage.put_task_in_archive(tag)
            return redirect('tasks/' + tag + ';' + request.user.username)

        elif 'remove' in request.POST:
            data_storage.remove_task(tag)
            return redirect('tasks')

        elif 'access' in request.POST:
            form = GiveAccessRightForm(request.POST)
            if form.is_valid():
                try:
                    data_storage.give_access_to(tag, form.cleaned_data['name'],
                                                AccessType.readonly)
                except AccessToYourself:
                    LOGGER.warning('Trying to give access to yourself! User: %s, task: %s',
                                   user.username, tag)

        elif 'reset' in request.POST:
            form = GiveAccessRightForm(request.POST)
            if form.is_valid():
                data_storage.user_storage.remove_access_right(tag, form.cleaned_data['name'])
                try:
                    notyfication = 'User {} took away your access to the task {}'.format(user.username, tag)
                    data_storage.send_notification([form.cleaned_data['name']],
                                                   notyfication)
                    data_storage.user_storage.save_state()
                except User.DoesNotExist:
                    pass

        elif 'edit' in request.POST:
            return redirect('edit', tag=tag)

        elif 'link' in request.POST:
            tasks = data_storage.get_tasks()
            all_tasks = [(task.tag, task.tag) for task in tasks if task.tag != tag]
            form = LinkForm(all_tasks, request.POST)
            if form.is_valid():
                data_storage.link_task_with(None, tag, form.cleaned_data['tag'],
                                            form.cleaned_data['type_link'])
                return redirect('tasks')

        elif 'reset_link' in request.POST:
            data_storage.break_link_with_task(tag)
            return redirect('tasks')

        LOGGER.info('User %s Show task %s', request.user.username, tag)

        form = GiveAccessRightForm()
        tasks = data_storage.get_tasks()
        all_tasks_tags = [(task.tag, task.tag) for task in tasks if task.tag != tag]
        lform = LinkForm(all_tasks_tags)
        observers = data_storage.user_storage.get_users_with_access_to(tag, user)
        context = {'username': request.user.username,
                   'task': task,
                   'form': form,
                   'observers': observers,
                   'lform': lform}

        return render(request, 'tasktracker/task.html', context)
    return redirect('main')


def get_manager_info(request, tag):
    if request.user.is_authenticated():
        user = request.user
        data_storage = DataStorageManager(ORMTaskStorage(user),
                                          ORMUserStorage(user),
                                          ORMTaskManagerStorage(user),
                                          user.username)
        tag = tag.split(';')[0].replace('_',' ')
        if request.method == 'POST':
            if 'remove' in request.POST:
                data_storage.remove_manager_task(tag)
                return redirect('tasks_manangers')
            elif 'edit' in request.POST:
                return redirect('edit_manager', tag=tag)
        manager = data_storage.task_manager_storage.get_manager_task_by_tag(tag)

        LOGGER.info('User %s showed info %s - manager task',
                    request.user.username, tag)

        context = {'username': user.username, 'manager': manager}
        return render(request, 'tasktracker/task_manager.html', context)
    return redirect('main')


def editing_task(request, tag):
    if request.user.is_authenticated():
        data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                          ORMUserStorage(request.user),
                                          ORMTaskManagerStorage(request.user),
                                          request.user.username)
        if request.method == 'POST':
            if 'save' in request.POST:
                form = CreateTaskForm(request.POST)
                if form.is_valid():
                    task = Task.objects.get(group__user=request.user, tag=tag)
                    old_tag = tag
                    task.update_from_form(form, request.user)
                    data_storage.update_tag_in_access_right(old_tag,
                                                            form.cleaned_data['tag'])
                    observers = [item.name for item in data_storage.user_storage.get_users_with_access_to(tag, request.user)]
                    data_storage.user_storage.save_state()
                    notyfication = 'The user {0} made changes to the "{1}"'.format(request.user.username, tag)
                    data_storage.send_notification(observers, notyfication)
                    data_storage.user_storage.save_state()

                    LOGGER.info('User %s change task: %s',request.user.username, tag)

                    return redirect('tasks')
        task = data_storage.get_task_by_tag(tag.split(';')[0].replace('_',' '))
        task_data = {'tag': task.tag, 'description': task.description,
                     'priority': task.status.priority.name,
                     'date_end': task.timing.end, 'date_begin': task.timing.begin}
        form = CreateTaskForm(task_data)

        context =  {'username': request.user.username, 'tag': task.tag, 'form': form}
        return render(request, 'tasktracker/edit_task.html', context)
    return redirect('main')


def editing_manager(request, tag):
    if request.user.is_authenticated():
        data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                          ORMUserStorage(request.user),
                                          ORMTaskManagerStorage(request.user),
                                          request.user.username)
        if request.method == 'POST':
            if 'save' in request.POST:
                form = CreateTaskManagerForm(request.POST)
                if form.is_valid():
                    manager = Mt.objects.get(tag=tag, author=request.user)
                    manager.update_from_form(form)

                    LOGGER.info('User %s change task managet: %s',
                                request.user.username, tag)
                    return redirect('tasks_manangers')

        tag = tag.split(';')[0].replace('_',' ')
        manager = data_storage.task_manager_storage.get_manager_task_by_tag(tag)
        initial_data = {'tag': manager.tag, 'description': manager.description,
                        'type_period': manager.period.split(' ')[0],
                        'period': manager.period.split(' ')[1],
                        'date_begin': manager.date_begin,
                        'date_end': manager.date_end}
        form = CreateTaskManagerForm(initial_data)

        context = {'username': request.user.username, 'tag': manager.tag, 'form': form}
        return render(request, 'tasktracker/edit_task.html', context)

    return redirect('main')

#Fix
def create_board(request):
    if request.user.is_authenticated():
        already = False
        if request.method == 'POST':
            form = GroupTasksForm(request.POST)
            if form.is_valid():
                try:
                    GroupTasks.objects.get(name=form.cleaned_data['name'], user=request.user)
                    already = True
                    name = form.cleaned_data['name']
                except GroupTasks.DoesNotExist:
                    LOGGER.info('User {0} create new board {1}'.format(request.user.username,
                                                                       form.cleaned_data['name']))

                    GroupTasks.objects.create(name=form.cleaned_data['name'], user=request.user)
                    return redirect('tasks')
        form = GroupTasksForm()
        if already:
            message = 'A group named: "' + name + '" already exists'
        else:
            message = ''
        return render(request, 'tasktracker/create_board.html', {'form': form,
                                                                 'username': request.user.username,
                                                                 'message': message})

    return redirect('main')

#Fix
def remove_board(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            if 'remove' in request.POST:
                group_name = request.POST['remove']
                data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                                  ORMUserStorage(request.user),
                                                  ORMTaskManagerStorage(request.user),
                                                  request.user.username)
                if len(data_storage.get_tasks('group', group_name)) == 0:
                    LOGGER.info('User %s removed group %s',
                                request.user.username, group_name)

                    GroupTasks.objects.get(user=request.user, name=group_name).delete()
        groups = GroupTasks.objects.filter(user=request.user)

        context = {'groups': groups, 'username': request.user.username}
        return render(request, 'tasktracker/remove_board.html', context)

    return redirect('main')


def add_task(request, group):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = CreateTaskForm(request.POST)
            if form.is_valid():
                data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                                  ORMUserStorage(request.user),
                                                  ORMTaskManagerStorage(request.user),
                                                  request.user.username)

                timing = Tm(form.cleaned_data['date_end'], form.cleaned_data['date_begin'])
                status = St(Priority[form.cleaned_data['priority']])
                task = Ts(timing, form.cleaned_data['description'],
                          form.cleaned_data['tag'], status=status)

                data_storage.add_task(task, group[1:])
                return redirect('tasks')

        form = CreateTaskForm()
        context = {'form': form, 'username': request.user.username, 'group': group[1:]}
        return render(request, 'tasktracker/add_task.html', context)

    return redirect('main')


def add_task_manager(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            form = CreateTaskManagerForm(request.POST)
            if form.is_valid():
                data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                                  ORMUserStorage(request.user),
                                                  ORMTaskManagerStorage(request.user),
                                                  request.user.username)
                task_manager = ManagerTask(form.cleaned_data['date_begin'],
                                           form.cleaned_data['date_end'],
                                           form.cleaned_data['description'],
                                           form.cleaned_data['tag'],
                                           (form.cleaned_data['type_period'] +
                                            ' ' + str(form.cleaned_data['period'])))
                data_storage.add_manager_task(task_manager)
                return redirect('tasks')

            context =  {'form': form,'username': request.user.username}
            return render(request, 'tasktracker/create_task_manager.html', context)

        form = CreateTaskManagerForm()
        context = {'form': form, 'username': request.user.username}
        return render(request, 'tasktracker/create_task_manager.html', context)

    return redirect('main')


def get_task_managers(request):
    if request.user.is_authenticated():
        data_storage = DataStorageManager(ORMTaskStorage(request.user),
                                          ORMUserStorage(request.user),
                                          ORMTaskManagerStorage(request.user),
                                          request.user.username)
        task_managers = data_storage.task_manager_storage.get_all_task_managers()

        LOGGER.info(' User %s get task managers', request.user.username)

        context = {'mangers': task_managers, 'username': request.user.username}
        return render(request, 'tasktracker/tasks_manangers.html', context)

    return redirect('main')
