from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_user_info, name='main'),
    url(r'^register/$', views.RegisterFormView.as_view(), name='reg'),
    url(r'^login/$', views.LoginFormView.as_view(), name='login'),
    url(r'^search/$', views.search_tasks, name='search'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^tasks/$', views.get_boards, name='tasks'),
    url(r'^userinfo/$', views.post_user_info, name='userinfo'),
    url(r'^userinfo/notifications$', views.get_messages, name='notifications'),
    url(r'^tasks/create_board/$', views.create_board),
    url(r'^tasks/remove_board/$', views.remove_board),
    url(r'^tasks/archiv/$', views.get_archiv, name='archiv'),
    url(r'^tasks/access/$', views.get_access, name='available'),
    url(r'^tasks/access/(?P<info>.*)/$', views.post_other_task),
    url(r'^tasks_manangers/$', views.get_task_managers, name='tasks_manangers'),
    url(r'^tasks_manangers/create/$', views.add_task_manager),
    url(r'^tasks_manangers/(?P<tag>.*)/edit/$', views.editing_manager, name='edit_manager'),
    url(r'^tasks_manangers/(?P<tag>.*)/$', views.get_manager_info),
    url(r'^tasks/add(?P<group>.*)/$', views.add_task),
    url(r'^tasks/(?P<tag>.*)/edit/$', views.editing_task, name='edit'),
    url(r'^tasks/(?P<tag>.*)/$', views.get_task_info),
]
