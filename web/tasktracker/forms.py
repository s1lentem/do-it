from django import forms
from .models import (GroupTasks, Task, Timing, Status, Comment)
from datetime import datetime
from dateutil import parser

class GroupTasksForm(forms.Form):
    name = forms.CharField(max_length=60)


class CreateTaskForm(forms.Form):
    tag = forms.CharField(max_length=20)
    description = forms.CharField(max_length=400)
    priority = forms.ChoiceField(choices=(('low', 'Low'),
                                          ('medium', 'Medium'),
                                          ('height', 'Height'),
                                          ('critical', 'Critical')))
    date_begin = forms.DateTimeField()
    date_end = forms.DateTimeField()


class CreateTaskManagerForm(forms.Form):
    tag = forms.CharField(max_length=20)
    description = forms.CharField(max_length=400)
    date_begin = forms.DateTimeField()
    date_end = forms.DateTimeField()
    period = forms.IntegerField(min_value=1)
    type_period = forms.ChoiceField(choices=(('day', 'Day'),
                                             ('week', 'Week'),
                                             ('month', 'Month')))

    def clean(self):
        date = self.cleaned_data['date_begin']
        if datetime(date.year, date.month, date.day, hour=date.hour, minute=date.minute) < datetime.now():
            self.add_error('date_begin', 'Start date can not be less than current date')
        if self.cleaned_data['date_end'] < date:
            self.add_error('date_end', 'End date can not be earlier than start date')

class GiveAccessRightForm(forms.Form):
    name = forms.CharField(required=False)


class FilterTasksForm(forms.Form):
    tag = forms.CharField(max_length=20, required=False)
    description = forms.CharField(max_length=400, required=False)
    priority = forms.ChoiceField(choices=(('none', 'none'),
                                          ('low', 'Low'),
                                          ('medium', 'Medium'),
                                          ('height', 'Height'),
                                          ('critical', 'Critical')))
    status = forms.ChoiceField(choices=(('none', 'None'),
                                        ('not_started', 'Not started'),
                                        ('durning', 'Durning'),
                                        ('completed', 'Completed'),
                                        ('failed', 'Failed')))
    deadline = forms.DateTimeField(required=False)
    search_scope = forms.ChoiceField(choices=(('all', 'All'),
                                              ('archiv', 'Only in the archive'),
                                              ('not_archiv', 'Not in archive')))


class LinkForm(forms.Form):
    type_link = forms.ChoiceField(label='how',choices=(('parallel', 'Parallel'),
                                                       ('stoped', 'Stoped'),
                                                       ('subtask', 'Subtask')))

    def __init__(self, tasks, *args, **kwargs):
        super(LinkForm, self).__init__(*args, **kwargs)
        self.fields['tag'] = forms.ChoiceField(choices=tasks, label='link to')
        self.fields['type_link'] = forms.ChoiceField(label='How',
                                                     choices=(('parallel', 'Parallel'),
                                                              ('stoped', 'Stoped'),
                                                              ('subtask', 'Subtask')))
