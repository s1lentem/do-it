from datetime import datetime

from django.contrib.auth.models import User
from django.utils.timezone import localtime

from tasktracklib.storages.storage_interface import (ITaskStorage,
                                                     IUserStorage,
                                                     ITaskManagerStorage)
from tasktracklib.components.taskcomp import (DependenceType, Priority,
                                              StatusType, Timing, Dependence,
                                              Status)
from tasktracklib.components.task import Task, GroupTasks, ManagerTask
from tasktracklib.components.user import AccessRights, AccessType, UserInfo


import tasktracker.models as models

class ORMTaskStorage(ITaskStorage):
    def __init__(self, current_user):
        self.current_user = current_user
        self.groups = list()

    def __get_full_task_from_orm(self, task):
        timing_model = models.Timing.objects.get(task=task)
        status_model = models.Status.objects.get(task=task)

        end = datetime(timing_model.end.year, timing_model.end.month,
                       timing_model.end.day, hour=timing_model.end.hour,
                       minute=timing_model.end.minute)
        begin = datetime(timing_model.begin.year, timing_model.begin.month,
                       timing_model.begin.day, hour=timing_model.begin.hour,
                       minute=timing_model.begin.minute)
        timing = Timing(end, begin)
        timing.change = timing_model.change
        timing.create = timing_model.create

        try:
            status = Status(Priority[status_model.priority.split('.')[1]],
                            StatusType[status_model.state.split('.')[1]])
        except IndexError:
            status = Status(Priority[status_model.priority])

        try:
            dependece_model = models.Dependence.objects.get(task=task)
            dependece = Dependence(dependece_model.task_tag,
                                   DependenceType[dependece_model.type_dependece],
                                   dependece_model.master)
        except models.Dependence.DoesNotExist:
            dependece = None

        task_obj = Task(timing, task.description, task.tag, list(), status,
                        dependece, task.types_notification)
        task_obj.model = task
        task_obj.group_name = task.group.name
        return task_obj


    def __update_or_create_model_from_task(self, task, group_name,
                                           old_task_model=None):
        task_model = models.Task() if old_task_model is None else old_task_model
        task_model.tag = task.tag
        task_model.description = task.description
        task_model.types_notification = task.types_notification
        task_model.group=models.GroupTasks.objects.get(name=group_name,
                                                       user=self.current_user)
        task_model.save()

        if old_task_model is None:
            timing_model = models.Timing()
        else:
            timing_model = models.Timing.objects.get(task=old_task_model)
        timing_model.begin = task.timing.begin
        timing_model.end = task.timing.end
        timing_model.change = task.timing.change
        timing_model.create = task.timing.create
        timing_model.task = task_model
        timing_model.save()

        if old_task_model is None:
            status_model = models.Status()
        else:
            status_model = models.Status.objects.get(task=task_model)
        status_model.priority = task.status.priority
        status_model.state = task.status.state
        status_model.task = task_model
        status_model.save()

        if task.dependencies is not None:
            if old_task_model is None:
                dependence_model = models.Dependence()
            else:
                try:
                    dependence_model = models.Dependence.objects.get(task=task_model)
                except models.Dependence.DoesNotExist:
                    dependence_model = models.Dependence()
            dependence_model.task_tag = task.dependencies.task_tag
            dependence_model.type_dependece = task.dependencies.type.name
            dependence_model.master = task.dependencies.master
            dependence_model.task = task_model
            dependence_model.save()
        else:
            try:
                dependence_model = models.Dependence.objects.get(task=task_model)
                dependence_model.delete()
            except models.Dependence.DoesNotExist:
                pass


    def __remove_task_from_orm(self, model):
        timing_model = models.Timing.objects.get(task=model)
        status_model = models.Status.objects.get(task=model)
        try:
            dependece_model = models.Dependence.objects.get(task=model)
            dependece_model.delete()
        except models.Dependence.DoesNotExist:
            pass

        timing_model.delete()
        status_model.delete()
        model.delete()


    def get_task_by_tag(self, tag, user=None):
        if user is None:
            user = self.current_user
        if isinstance(user, str):
            user = User.objects.get(username=user)
        try:
            task = models.Task.objects.get(tag=tag, group__user=user)
        except models.Task.DoesNotExist:
            return None
        group = self.get_group(task.group.name, user)
        for task_info in group.tasks:
            if task_info.tag == tag:
                return (task_info, group)



    def get_group(self, group_name, user=None):
        if user is None:
            user = self.current_user
        try:
            group_model = models.GroupTasks.objects.get(name=group_name,
                                                        user=user)
        except models.GroupTasks.DoesNotExist:
            if group_name == 'archiv' or group_name == 'default':
                group_model = models.GroupTasks.objects.create(name=group_name,
                                                               user=user)
        group = GroupTasks(group_model.name)
        self.groups.append(group)
        all_tasks_models = models.Task.objects.filter(group=group_model,
                                                      group__user=user)
        for task_model in all_tasks_models:
            task = self.__get_full_task_from_orm(task_model)
            group.add_task(task)
        return group


    def get_all_tasks(self):
        all_groups = models.GroupTasks.objects.filter(user=self.current_user
                                                      ).exclude(name='archiv')
        if all_groups.count() == 0:
            return list()
        all_tasks = list()
        for group in all_groups:
            all_tasks = all_tasks + self.get_group(group.name).tasks
        return all_tasks

    def tag_is_not_busy(self, tag):
        all_tasks = models.Task.objects.filter(group__user=self.current_user)
        return not (tag in [task.tag for task in all_tasks])

    def get_archived_tasks(self):
        return self.get_group('archiv')


    def save_state(self):
        for group in self.groups:
            for task in group.tasks:
                try:
                    if task.group_name != group.name:
                        raise AttributeError()
                    self.__update_or_create_model_from_task(task, group.name, task.model)
                except AttributeError:
                    self.__update_or_create_model_from_task(task, group.name)

            tasks_models = models.Task.objects.filter(group__name=group.name,
                                                      group__user=self.current_user)


            tag_for_remove = (set([model.tag for model in tasks_models]) -
                              set([task.tag for task in group.tasks]))
            for task_model in (model for model in tasks_models if model.tag in tag_for_remove):
                self.__remove_task_from_orm(task_model)

            self.groups = list()


class ORMUserStorage(IUserStorage):
    def __init__(self, current_user):
        self.user = current_user
        self.all_access_right = list()
        self.all_users = list()

    def __get_access_right_from_models(self, model):
        if '.' in model.access_type:
            access_type = model.access_type.split('.')[1]
        else:
            access_type = model.access_type
        access_right = AccessRights(model.master, model.user, model.tag, access_type)
        access_right.model = model
        self.all_access_right.append(access_right)
        return access_right


    def __copy_access_in_model_and_save(self, access, model=None):
        if model is None:
            model = models.AccessRights()
        model.master = access.master
        model.user = access.user
        model.tag = access.tag
        model.acc_type = access.type.name
        model.save()

    def __get_user_info_from_access_model(self, model):
        if isinstance(model, str):
            user_name = model
        else:
            user_name = model.username
        user_model = User.objects.get(username=user_name)
        messages = models.Notification.objects.filter(user=user_model)
        user_info = UserInfo(user_name, None, [item.message for item in messages])
        user_info.model = model
        self.all_users.append(user_info)
        return user_info

    def get_all_access_manager(self):
        all_access_models = models.AccessRights.objects.all()
        for model in all_access_models:
            self.__get_access_right_from_models(model)
        return self.all_access_right


    def get_users_with_access_to(self, tag, task_master=None):
        if task_master is None:
            task_master = self.user
        all_models = models.AccessRights.objects.filter(tag=tag, master=task_master)
        reslut = list()
        for model in all_models:
            reslut.append(self.__get_user_info_from_access_model(model.user))
        return reslut


    def get_access_right_for_master_with_tag(self, tag, master=None):
        if master is None:
            master = self.user
        all_access_models = models.AccessRights.objects.filter(tag=tag, master=master)
        for model in all_access_models:
            self.__get_access_right_from_models(model)
        return self.all_access_right


    def remove_access_right(self, task_tag, user=None):
        try:
            if user is None:
                result = models.AccessRights.objects.filter(tag=task_tag,
                                                            master=self.user)
                result.delete()
            else:
                if isinstance(user, str):
                    user = User.objects.get(username=user)
                result = models.AccessRights.objects.filter(tag=task_tag,
                                                            master=self.user,
                                                            user=user)
                result.delete()
        except User.DoesNotExist:
            pass

    def get_tasks_are_available_for(self, user_name):
        all_models = models.AccessRights.objects.filter(user=user_name)
        for model in all_models:
            self.__get_access_right_from_models(model)
        return self.all_access_right


    def give_access_right_to(self, access_right_info):
        try:
            User.objects.get(username=access_right_info.user)
            models.AccessRights.objects.get(master=access_right_info.master,
                                            user=access_right_info.user,
                                            tag=access_right_info.tag,
                                            access_type=access_right_info.type.name)
        except models.AccessRights.DoesNotExist:
            models.AccessRights.objects.create(master=access_right_info.master,
                                               user=access_right_info.user,
                                               tag=access_right_info.tag,
                                               access_type=access_right_info.type.name)
        except User.DoesNotExist:
            pass


    def get_user(self, user_name):
        messages = models.Notification.objects.filter(user=self.user)
        user_info = UserInfo(user_name, '', [message.message for message in messages])
        self.all_users.append(user_info)
        return user_info


    def is_duplicate_user_name(self, user_name):
        raise NotImplementedError()


    def add_new_user(self, user):
        raise NotImplementedError()


    def save_state(self):
        for item in self.all_access_right:
            try:
                self.__copy_access_in_model_and_save(item, item.model)
            except AttributeError:
                self.__copy_access_in_model_and_save(item)


        for user in self.all_users:
            for message in user.messages:
                try:
                    models.Notification.objects.get(user__username=user.name,
                                                   message=message)
                except models.Notification.DoesNotExist:
                    try:
                        models.Notification.objects.create(user=User.objects.get(username=user.name),
                                                           message=message)
                    except User.DoesNotExist:
                        pass

            for model in models.Notification.objects.filter(user__username=user.name):
                if not (model.message in user.messages):
                    model.delete()


class ORMTaskManagerStorage(ITaskManagerStorage):

    def __init__(self, current_user):
        self.current_user = current_user
        self.all_managers = list()

    def __get_full_task_manager_from_orm(self, task_manager):
        manager = ManagerTask(task_manager.date_begin, task_manager.date_end,
                           task_manager.description, task_manager.tag,
                           task_manager.period, task_manager.last_date)
        manager.model = task_manager
        self.all_managers.append(manager)
        return manager

    def __copy_task_manager_in_model_and_save(self, task_manager, model=None):
        if model is None:
            model = models.ManagerTask()
        model.tag = task_manager.tag
        model.date_begin = task_manager.date_begin
        model.date_end = task_manager.date_end
        model.description = task_manager.description
        model.period = task_manager.period
        model.last_date = task_manager.last_date
        model.author = self.current_user
        model.save()

    def get_all_task_managers(self):
        all_task_managers_models = models.ManagerTask.objects.filter(author=self.current_user)
        for model in all_task_managers_models:
            self.__get_full_task_manager_from_orm(model)
        return self.all_managers

    def get_manager_task_by_tag(self, tag):
        model = models.ManagerTask.objects.get(author=self.current_user, tag=tag)
        return self.__get_full_task_manager_from_orm(model)


    def save_state(self):
        for item in self.all_managers:
            try:
                self.__copy_task_manager_in_model_and_save(item, item.model)
            except AttributeError:
                self.__copy_task_manager_in_model_and_save(item)

        all_managers_models = models.ManagerTask.objects.filter(author=self.current_user)
        managers_for_removed = (set([model.tag for model in all_managers_models]) -
                                set([manager.tag for manager in self.all_managers]))
        for model in [item for item in all_managers_models if item.tag in managers_for_removed]:
            model.delete()
