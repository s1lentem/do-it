from django.db import models
from django.utils import timezone
from datetime import datetime


class GroupTasks(models.Model):
    name = models.CharField(max_length=100, default='default')
    user = models.ForeignKey('auth.User', null=False)

    def __str__(self):
        return self.name

class Notification(models.Model):
    message = models.TextField()
    user = models.ForeignKey('auth.User', null=False)


class Task(models.Model):
    tag = models.CharField(max_length=100)
    description = models.TextField()
    group = models.ForeignKey(GroupTasks)
    types_notification = models.TextField()

    def __str__(self):
        return self.tag

    def update_from_form(self, form, user):
        st_model = Status.objects.get(task__group__user=user, task__tag=self.tag)
        tm_model = Timing.objects.get(task__group__user=user, task__tag=self.tag)
        st_model.priority = form.cleaned_data['priority']
        tm_model.begin = form.cleaned_data['date_begin']
        tm_model.end = form.cleaned_data['date_end']
        tm_model.change = datetime.now()
        self.tag = form.cleaned_data['tag']
        self.description = form.cleaned_data['description']
        st_model.save()
        tm_model.save()
        self.save()

class Timing(models.Model):
    begin = models.DateTimeField()
    end = models.DateTimeField(null=True)
    create = models.DateTimeField()
    change = models.DateTimeField(null=True)
    task = models.OneToOneField(Task)


class Dependence(models.Model):
    task_tag = models.CharField(max_length=100)
    type_dependece = models.CharField(max_length=100)
    master = models.CharField(max_length=100, null=True)
    task = models.OneToOneField(Task)


class Status(models.Model):
    state = models.CharField(max_length=13)
    priority = models.CharField(max_length=8)
    task = models.OneToOneField(Task)


class Comment(models.Model):
    author = models.ForeignKey('auth.User')
    date = models.DateTimeField(default=timezone.now)
    message = models.TextField()
    task = models.ForeignKey(Task)


class ManagerTask(models.Model):
    author = models.ForeignKey('auth.User')
    tag = models.CharField(max_length=100)
    date_begin = models.DateTimeField()
    date_end = models.DateTimeField()
    description = models.TextField()
    period = models.TextField()
    last_date = models.DateTimeField(null=True)

    def update_from_form(self, form):
        self.tag = form.cleaned_data['tag']
        self.description = form.cleaned_data['description']
        self.date_begin = form.cleaned_data['date_begin']
        self.date_end = form.cleaned_data['date_end']
        self.period = (form.cleaned_data['type_period'] + ' ' +
                       str(form.cleaned_data['period']))
        self.save()


class AccessRights(models.Model):
    master = models.CharField(max_length=100)
    user = models.CharField(max_length=100)
    tag = models.CharField(max_length=100)
    access_type = models.CharField(max_length=100)
