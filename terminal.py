# !usr/bin/env python3

import sys

import source.terminal.parsargs as parser
import tasktracklib.dateparse as dp

from source.terminal.inindex import DuplicateIndexError
from tasktracklib.errors import (NonExistentArgumentError,
                        InvalidNumberOfArgumentsError,
                        DuplicateTagError)
from source.applogger import get_log_for_app

LOGGER = get_log_for_app(None, 'INFO')

def main():
    try:
        args = sys.argv[1::]
        parser.parser(args)
    except NonExistentArgumentError:
        LOGGER.error('A non-existent command was given ' + args[0])
        print('Команды ' + args[0] + ' не существует')
    except InvalidNumberOfArgumentsError:
        LOGGER.error('The command "' + args[0] + '" must have a different '
                     'number of parameters')
        print('Команда ' + args[0] + ' должна иметь другое кол-во параметров')
    except DuplicateTagError:
        LOGGER.error('An attempt was made to add a task with an existing tag')
        print('Тэг, в задаче которой вы пытались добавить, уже занят')
    except DuplicateIndexError:
        LOGGER.error('Attempt to create an index that already exists')
        print('Индекс уже существует, перейдите к его редактированию')


if __name__ == '__main__':
    main()
