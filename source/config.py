import configparser
import os

__all__ = ['get_path_for_log', 'get_path_for_users', 'get_auto_clear_messages',
           'get_type_notification']

CONFIG_NAME = os.path.join(os.path.expanduser('~'), '.disettings.ini')

def create_config(path=None):
    config = configparser.ConfigParser()
    config.add_section('settings')
    config.set('settings', 'path_for_users', 'None')
    config.set('settings', 'path_for_log', 'None')
    config.set('settings', 'level_log', 'INFO')
    config.set('settings', 'notification', '1 days|1 hours')
    config.set('settings', 'auto_clear_messages', 'true')
    config.set('settings', 'path_for_app_log')
    if path is None:
        path = CONFIG_NAME
    else:
        path = os.path.join(path, CONFIG_NAME)
    with open(path, 'w') as config_file:
        config.write(config_file)

def load_config(path=None):
    config = configparser.ConfigParser()
    if path is None:
        path = CONFIG_NAME
    else:
        path = os.path.join(path, CONFIG_NAME)
    if not os.path.exists(path):
        create_config()
    config.read(path)
    return config

def get_path_for_users(path=None):
    config = load_config(path)
    path_for_users = config.get('settings', 'path_for_users')
    if path_for_users == 'None':
        path_for_users = os.path.join(os.getcwd(), 'users')
    return path_for_users

def get_path_for_log(path=None):
    config = load_config(path)
    path_for_log = config.get('settings', 'path_for_log')
    if path_for_log == 'None':
        path_for_log = os.path.join(os.getcwd(), 'LIBLOG.log')
    return path_for_log

def get_path_for_app_log(path=None):
    config = load_config(path)
    path_for_log = config.get('settings', 'path_for_app_log')
    if path_for_log == 'None':
        path_for_log = os.path.join(os.getcwd(), 'command.log')
    return path_for_log

def get_auto_clear_messages(path=None):
    config = load_config(path)
    is_auto_clear_messages = config.get('settings', 'auto_clear_messages')
    if is_auto_clear_messages == 'true':
        return True
    return False

def get_type_notification(path=None):
    config = load_config(path)
    return config.get('settings', 'notification')

def get_level_for_log(path=None):
    config = load_config(path)
    return config.get('settings', 'level_log')
