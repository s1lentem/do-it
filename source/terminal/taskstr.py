from termcolor import colored

__all__ = ['get_str_from_task', 'get_detailed_description_task']

DATE_FORMAT = '%Y-%m-%d %H:%M'

def get_color_for_priority(priority):
    if priority.name == 'critical':
        return colored(priority.name, 'red')
    elif priority.name == 'height':
        return colored(priority.name, 'yellow')
    elif priority.name == 'medium':
        return colored(priority.name, 'green')
    return colored(priority.name, 'white')

def get_str_from_timing(timing):
    date_begin = 'date begin: ' + timing.begin.strftime(DATE_FORMAT)
    date_end = 'date end: ' + (timing.end.strftime(DATE_FORMAT) if
                               timing.end is not None else 'not')
    date_create = 'date created: ' + timing.create.strftime(DATE_FORMAT)
    dateb_change = 'date change: ' + (timing.change.strftime(DATE_FORMAT) if
                                      timing.change is not None else 'not')
    return date_begin + '\n' + date_end + '\n' + date_create + '\n' + dateb_change

def get_str_from_dependence(dependence):
    if dependence is not None:
        if dependence.master is not None:
            return ('Associated with ' + dependence.task_tag +
                    ' of the user "' + dependence.master + '" as ' +
                    dependence.type)
        return ('Associated with ' + dependence.task_tag + ' as ' +
                dependence.type.name)
    return 'Has no dependencies'

def get_str_from_status(status):
    if status.name == 'completed':
        return colored(status.name, 'green')
    elif status.name == 'failed':
        return colored(status.name, 'red')
    return colored(status.name, 'white')


def get_str_from_task(task):
    tag = task.tag
    datebegin = task.timing.begin.strftime(DATE_FORMAT)
    dateend = task.timing.end.strftime(DATE_FORMAT)
    descrip = task.description
    state = get_str_from_status(task.status.state)
    priority = get_color_for_priority(task.status.priority)
    return (tag + ' | ' + datebegin + ' --> ' + dateend + ' | ' +
            priority +  ' | '  +  state + ' | ' + descrip)

def get_detailed_description_task(task):
    result = str()
    all_dates = get_str_from_timing(task.timing)
    result += 'Tag: ' + task.tag + '\n'
    result += 'Description: ' + task.description + '\n'
    result += 'Priority: ' + get_color_for_priority(task.status.priority) + '\n'
    result += 'Status: ' + get_str_from_status(task.status.state) + '\n'
    result += all_dates + '\n'
    result += get_str_from_dependence(task.dependencies)

    return result
