import json
import os
import datetime

from tasktracklib.dateparse import *
from tasktracklib.components.task import ManagerTask, Timing, Task, Priority
from source.config import get_path_for_users

CURRENT_USER_FILE_NAME = '.current'

def creating_task_from_index(path_to_index):
    if os.path.exists(path_to_index):
        with open(path_to_index, 'r') as file:
            components = json.loads(file.read())
            timing = get_timing_from_index(components)
            descript = components['descript']
            tag = components['tag']
            task = Task(timing, descript, tag)
            if 'priority' in components:
                task.status.priority = Priority[components['priority']]
            if 'group' in components:
                group = components['group']
            else:
                group = 'default'
        return (task, group)
    raise NotImplementedError('Нету пути')

def creating_manager_from_index(dict_manager):
    period = dict_manager['type'] + ' ' + dict_manager['dimension']
    if not ('datebegin' in dict_manager):
        date_begin = datetime.now()
    else:
        date_begin = get_datetime(dict_manager['datebegin'])
    if 'dateend' in dict_manager:
        date_end = get_datetime(dict_manager['dateend'])
    else:
        date_end = None
    return ManagerTask(date_begin, date_end, dict_manager['descript'],
                           dict_manager['tag'], period)

def get_timing_from_index(dict_component):
    allDates = []
    if 'dateend' in dict_component:
        allDates.append(get_datetime(dict_component['dateend'].split(' ')))
    if 'datebegin' in dict_component:
        allDates.append(get_datetime(dict_component['datebegin'].split(' ')))
    if len(allDates) == 0:
        raise NotImplementedError('Нету нужны ключей')
    if len(allDates) == 1:
        return Timing(allDates[0])
    return Timing(allDates[0], allDates[1])

def get_info_from_index(path_to_index):
    if os.path.exists(path_to_index):
        res = str()
        with open(path_to_index, 'r') as file:
            info = json.loads(file.read())
            for key in info:
                res = str(res) + str(key) + ': ' + str(info[key]) + '\n'
        return res
    else:
        raise FileNotFoundError('Индекса нету')

def get_dict_from_index(path_to_file):
    if not os.path.exists(path_to_file):
        raise FileNotFoundError('Не сущ. пути ' + path_to_file)
    with open(path_to_file, 'r') as file:
        result = json.loads(file.read())
    return result

def set_current_user(path_to_all_users, user_name):
    full_path = os.path.join(path_to_all_users, CURRENT_USER_FILE_NAME)
    with open(full_path, 'w') as file:
        file.write(user_name)

def get_current_user():
    full_path = os.path.join(get_path_for_users(), CURRENT_USER_FILE_NAME)
    if os.path.exists(full_path):
        with open(full_path, 'r') as file:
            user_name = file.read()
        if user_name != '':
            return user_name
    else:
        with open(full_path, 'w'):
            pass
