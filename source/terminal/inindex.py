import os
import json
import datetime

from tasktracklib.errors import NonExistentArgumentError

MODIFICATION_DATE = ['begin', 'end']
TYPE_PERIOD = ['day', 'month', 'week']

class DuplicateIndexError(Exception):
    '''Попытка создания индекса, при условии что индекс уже существует'''
    pass

def encode_datetime(obj):
    if isinstance(obj, datetime.datetime):
        return '{}-{}-{} {}:{}'.format(obj.year, obj.month, obj.day,
                                    obj.hour, obj.minute)
    return obj.__dict__

def create_index_for_task(path_to_dir):
    '''Создает временный файл для
    добавление всей необходимой инофрмации о задаче'''
    file_name = 'temp.json'
    path = os.path.join(path_to_dir, file_name)
    if not os.path.exists(path):
        with open(path, 'w') as file:
            json.dump({'type': 'create'}, file)
        return path
    raise DuplicateIndexError()


def create_index_for_manager(path_to_dir):
    file_name = 'manager.json'
    path = os.path.join(path_to_dir, file_name)
    if not os.path.exists(path):
        with open(path, 'w') as file:
            json.dump({}, file)
        return path
    raise DuplicateIndexError()


def add_key_and_value_in_index(path_for_index, key, value):
    with open(path_for_index, 'r') as f:
        info = json.loads(f.read())
    with open(path_for_index, 'w') as f:
        info[key] = value
        json.dump(info, f, default=encode_datetime)


def add_tag_in_index(path_for_index, arg):
    add_key_and_value_in_index(path_for_index, 'tag', arg)

def add_dependence_in_index(path_for_index, tag, type_dependece, user):
    if not (type_dependece in [item.name for item in DependenceType]):
        raise NonExistentArgumentError()
    component = {'tag': tag, 'type': type_dependece, 'user': user }
    add_key_and_value_in_index(path_for_index, 'dependence', component)


def add_date_in_index(path_for_index, modification, date, func_parce_date):
    '''Аргументы даты передаются черз строку'''
    if not (modification in MODIFICATION_DATE):
        raise NonExistentArgumentError()
    try:
        date = func_parce_date(date[0])
        add_key_and_value_in_index(path_for_index, 'date' + modification, date)
    finally:
        pass

def add_priority_in_index(path_for_index, priority, enumPriority):
    if priority in [item.name for item in enumPriority]:
        add_key_and_value_in_index(path_for_index, 'priority', priority)
    else:
        raise NotImplementedError('Не подходящий тип')

def add_group_in_index(path_for_index, group_name):
    add_key_and_value_in_index(path_for_index, 'group', group_name)

def add_period_in_index(path_to_index, type_period, dimension):
    if not dimension.isdigit():
        raise ValueError('dimension должно быть целым числом')
    if not (type_period in TYPE_PERIOD):
        raise ValueError('type_period умел не верный формат')
    with open(path_to_index, 'r') as file:
        components = json.loads(file.read())
    components['type'] = type_period
    components['dimension'] = dimension
    with open(path_to_index, 'w') as file:
        json.dump(components, file)

def parse_in_index(dictTask):
    result = {'type': 'mod'}
    result['tag'] = dictTask['tag']
    result['descript'] = dictTask['description']
    result['dateend'] = dictTask['timing']['end']
    result['datebegin'] = dictTask['timing']['begin']
    result['priority'] = dictTask['status']['priority']
    result['users'] = dictTask['observers']
    return result
