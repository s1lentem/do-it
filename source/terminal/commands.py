import os

from termcolor import colored

import source.terminal.inindex as indx
import source.terminal.fromindex as fndx
import tasktracklib.errors as errors

from tasktracklib.components.user import AccessRights
from tasktracklib.components.task import Priority, StatusType
from tasktracklib.storages.jsonstorage.jsonstorage import (JSONTaskStorage,
                                                          JSONUserStorage,
                                                          JSONManagerTaskStorage)
from tasktracklib.dateparse import get_datetime
from tasktracklib.storagemanager import DataStorageManager

from source.config import (get_path_for_users, get_type_notification,
                           get_path_for_log, get_level_for_log,
                           get_path_for_app_log)
from source.terminal.taskstr import (get_str_from_task,
                                     get_detailed_description_task)
from source.arghelper import get_description_for_command
from source.applogger import get_log_for_app

CURRENT_USER = fndx.get_current_user()
DATA_STORAGE = DataStorageManager(JSONTaskStorage(CURRENT_USER),
                                  JSONUserStorage(CURRENT_USER),
                                  JSONManagerTaskStorage(CURRENT_USER),
                                  CURRENT_USER,
                                  get_path_for_log(), get_level_for_log())

NAME_TEMP_FILE = 'temp.json'
NAME_MANAGER_FILE = 'manager.json'

ADDED_TASK_FUNC = ['date', 'priority', 'tag', 'descript', 'group']
ADDED_MANAGER_FUNC = ['date', 'tag', 'descript', 'period']

LOGGER = get_log_for_app(None, 'INFO')

def print_project(project_tuple, space=0):
    print(space*'-', get_str_from_task(project_tuple.task_info))
    for task_typle in project_tuple.child_tasks:
        print_project(task_typle, space + 2)

def check_all_tasks_and_managers():
    DATA_STORAGE.check_tasks_from_managers()
    failed_task = DATA_STORAGE.check_deadline_for_each_task()
    if len(failed_task):
        print('\n','-'*50,'')
        print('Упс, кажется вы не уложились в сроки, вот список всех задач:'),
        for task in failed_task:
            print(get_str_from_task(task))

        print('\n','Может вы всё-таки уложились в сроки, но забыли это пометить?\n',
          'Если это так, то вы знаете что нужно делать =)','\n')
    messages = DATA_STORAGE.get_all_messages_user()
    for message in messages:
        print(message)
    all_tasks = DATA_STORAGE.checking_date_for_notification()
    if len(all_tasks):
        for task in all_tasks:
            print('Напоминаем, что ' + str(task.timing.end) + ' вы должны '
                  'заврешить ' + task.tag)

def add_manager_from_index(path_to_index):
    manager_dict = fndx.get_dict_from_index(path_to_index)
    manager = fndx.creating_manager_from_index(manager_dict)
    DATA_STORAGE.add_manager_task(manager)

def add_task_from_index(path_for_index):
    task = fndx.creating_task_from_index(path_for_index)
    task[0].types_notification = get_type_notification()
    DATA_STORAGE.add_task(task[0], task[1])

def authorization_command(path_to_all_users, user_name):
    password = input('Введите пороль: ')
    all_user = DATA_STORAGE.get_all_users_name()
    if user_name in all_user:
        user = DATA_STORAGE.get_user(user_name)
        if user.password == password:
            fndx.set_current_user(path_to_all_users, user_name)
            return True
    return False

def register_command(name, password):
    temp_psw = input('Пожалуйста, введите пороль ещё раз: ')
    if temp_psw == password:
        DATA_STORAGE.register_user(name, password)
        return True
    raise Exception('Не верный пороль')

def create_index_command(index_type):
    if index_type == 'task':
        indx.create_index_for_task(os.getcwd())
    elif index_type == 'manager':
        indx.create_index_for_manager(os.getcwd())
    else:
        raise TypeError('Строка имела не верный формат: ' + index_type)

def added_from_index_command(index_type):
    if index_type == 'task':
        path = os.path.join(os.getcwd(), NAME_TEMP_FILE)
        if os.path.exists(path):
            add_task_from_index(path)
            os.remove(path)
        else:
            raise FileNotFoundError('Индекс не найден')
    elif index_type == 'manager':
        path = os.path.join(os.getcwd(), NAME_MANAGER_FILE)
        if os.path.exists(path):
            add_manager_from_index(path)
            os.remove(path)
        else:
            raise FileNotFoundError('Не существует индекса')

def reset_command(reset_type, tag=None, user=None):
    if reset_type in ['task', 'manager']:
        if reset_type == 'task':
            path = os.path.join(os.getcwd(), NAME_TEMP_FILE)
        elif reset_type == 'manager':
            path = os.path.join(os.getcwd(), NAME_MANAGER_FILE)
        if os.path.exists(path):
            os.remove(path)
    if reset_type == 'link':
        DATA_STORAGE.break_link_with_task(tag)
    elif reset_type == 'access':
        DATA_STORAGE.get_away_access_from(tag, user)

def show_command(show_type, param):
    if show_type == 'all':
        all_tasks = DATA_STORAGE.get_tasks()
        for task in all_tasks:
            print(get_str_from_task(task))
        available_task = DATA_STORAGE.get_tasks_are_available_for()
        for task in available_task:
            print(task[1])
            print(get_str_from_task(task[0]))
    elif show_type == 'arch':
        archiv = DATA_STORAGE.get_tasks('arch')
        for task in archiv.tasks:
            print(get_str_from_task(task))
    elif show_type in ['group', 'status', 'priority']:
        if show_type == 'status':
            param = StatusType[param]
        if show_type == 'priority':
            param = Priority[param]
        tasks = DATA_STORAGE.get_tasks(show_type, param)
        for task in tasks:
            print(get_str_from_task(task))
    elif show_type == 'project':
        root_task = DATA_STORAGE.get_task_by_tag(param)
        project = DATA_STORAGE.get_project_from(root_task)
        print_project(project)
    elif show_type == 'task':
        task = DATA_STORAGE.get_task_by_tag(param)
        if task is not None:
            print(get_detailed_description_task(task))


def status_command(status_type):
    if status_type == 'task':
        print(fndx.get_info_from_index(NAME_TEMP_FILE))
    elif status_type == 'manager':
        print(fndx.get_info_from_index(NAME_MANAGER_FILE))
    elif status_type == 'user':
        print(CURRENT_USER + '\n')

def archiving_command(tag):
    DATA_STORAGE.put_task_in_archive(tag)

def add_task_component_command(args):
    attr = args['attr']
    path = os.path.join(os.getcwd(), NAME_TEMP_FILE)
    if attr in ADDED_TASK_FUNC:
        if attr == 'date':
            indx.add_date_in_index(path, args['type'],
                                   [args['date'], args['time']], get_datetime)
        if attr == 'tag':
            indx.add_tag_in_index(path, args['value'])
        if attr == 'priority':
            indx.add_priority_in_index(path, args['value'], Priority)
        if attr == 'descript':
            indx.add_key_and_value_in_index(path, attr, args['value'])
        if attr == 'group':
            indx.add_group_in_index(path, args['value'])

def add_manager_component_command(args):
    attr = args['attr']
    path = os.path.join(os.getcwd(), NAME_MANAGER_FILE)
    if attr in ADDED_MANAGER_FUNC:
        if attr == 'date':
            indx.add_date_in_index(path, args['type'],
                                   [args['date'], args['time']], get_datetime)
        elif attr == 'tag':
            indx.add_tag_in_index(path, args['value'])
        elif attr == 'priority':
            indx.add_period_in_index(path, args['value'], Priority)
        elif attr == 'descript':
            indx.add_key_and_value_in_index(path, attr, args['value'])
        elif attr == 'period':
            indx.add_period_in_index(path, args['type'], args['interval'])
    else:
        raise ValueError('Не верный аргумент')

def get_valid_value(type_value, value):
    if type_value == 'priority':
        if isinstance(value, str):
            return Priority[value]
        else:
            return Priority(values)
    if type_value == 'date':
        return parse_datetime(value)
    return value

def edit_task_command(user_name, tag, field, value):
    DATA_STORAGE.change_task(tag, field, value, user_name)

def editing_someone_else_task(master, tag, field, value):
    DATA_STORAGE.editing_someone_else_task(master, tag, field, value)

def link_to_command(user, master_tag, sub_tag, link_type):
    DATA_STORAGE.link_task_with(user, master_tag, sub_tag, link_type)

def access_command(user, tag, acc_type):
    DATA_STORAGE.give_access_to(tag, user, acc_type)

def complete_task_command(tag):
    DATA_STORAGE.complete_task(tag)

def remove_command(removed_type, tag):
    if removed_type == 'task':
        DATA_STORAGE.remove_task(tag)
    elif removed_type == 'manager':
        DATA_STORAGE.remove_manager_task(tag)
    else:

        LOGGER.errors('Invalid parameter for remove ' + removed_type)

        raise NotImplementedError()

def help_command(command=None):
    print(get_description_for_command(command))

def call_command(args):
    try:
        user = DATA_STORAGE.current_user
        command = args['command']

        LOGGER.info('The ' + command +' command was called with parameters' +
                    str(args.values))

        if command == 'reg':
            register_command(args['name'], args['passwd'])
        elif command == 'login':
            authorization_command(get_path_for_users(), args['name'])
        elif user is None:
            raise NotImplementedError('Пользователь не найден')
        elif command == 'create':
            create_index_command(args['type'])
        elif command == 'reset':
            reset_command(args['type'], args['tag'], args['user'])
        elif command == 'show':
            show_command(args['type'], args['param'])
        elif command == 'status':
            status_command(args['type'])
        elif command == 'modt':
            add_task_component_command(args)
        elif command == 'modm':
            add_manager_component_command(args)
        elif command == 'add':
            added_from_index_command(args['type'])
        elif command == 'arch':
            archiving_command(args['tag'])
        elif command == 'edit':
            edit_task_command(user, args['tag'], args['attr'], args['value'])
        elif command == 'editother':
            editing_someone_else_task(args['master'], args['tag'], args['field'],
                                      args['value'])
        elif command == 'link':
            link_to_command(args['user'], args['master_tag'], args['sub_tag'], args['type'])
        elif command == 'access':
            access_command(args['user'], args['tag'], args['type'])
        elif command == 'compl':
            complete_task_command(args['tag'])
        elif command == 'remove':
            remove_command(args['type'], args['tag'])
        elif command == 'help':
            help_command(args['type_command'])
        check_all_tasks_and_managers()
    except errors.IncorrectDateError as er:
        print(er.message)
    except errors.EndTaskGeneration as er_info:
        print('У менеджера задач ' + er_info.manager.tag + ' закончился ппериод'
              ' для формирования задач!')

        LOGGER.warning('The task manager %s has completed the dates for '
                       'processing tasks', er_info.manager.tag)
