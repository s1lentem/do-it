from enum import Enum

from tasktracklib.dateparse import get_datetime
from source.terminal.commands import call_command

from tasktracklib.errors import (NonExistentArgumentError,
                        InvalidNumberOfArgumentsError)

TYPE_INDEX = ['task', 'manager']
ADDED_TASK_FUNC = ['date', 'dep', 'priority', 'user', 'tag', 'descript', 'group']
MODIFICATION_DATE = ['begin', 'end']
ADDED_MANAGER_FUNC = ['date', 'tag', 'descript', 'period']



class CommandType(Enum):
    add = 0
    access = 1
    modt = 2
    modm = 3
    compl = 4
    status = 5
    reg = 6
    create = 7
    reset = 8
    edit = 9
    show = 10
    arch = 11
    login = 12
    link = 13
    remove = 14
    editother = 15
    help = 16


def check_len_args(args, type_command):
    len_args = len(args)
    if type_command in [CommandType.login, CommandType.create, CommandType.add,
                        CommandType.status, CommandType.arch, CommandType.compl]:
        return len_args == 1
    elif type_command is CommandType.reg:
        return len_args == 2
    elif type_command is CommandType.modt:
        if 'date' in args:
            return len_args == 3 or len_args == 4
        return len_args == 2
    elif type_command is CommandType.modm:
        if 'date' in args:
            return len_args == 3 or len_args == 4
        if 'period' in args:
            return len_args == 3
        return len_args == 2
    elif type_command is CommandType.edit:
        if args[1] in ['dateend', 'datebegin']:
            return len_args == 3 or len_args == 4
        return len_args == 3
    elif type_command is CommandType.editother:
        if args[2] in ['dateend', 'datebegin']:
            return len_args == 4 or len_args == 5
        return len_args == 4
    elif type_command is CommandType.access:
        return len_args == 3
    elif type_command is CommandType.link:
        return len_args >= 2 and len_args <= 4
    elif type_command is CommandType.reset:
        if args[0] in ['task', 'manager']:
            return len_args == 1
        elif args[0] == 'link':
            return len_args == 2
        return len_args == 3
    elif type_command is CommandType.show:
        if args[0] in ['all', 'arch']:
            return len_args == 1
        return len_args == 2
    elif type_command is CommandType.remove:
        return len_args == 2
    elif type_command is CommandType.help:
        return len_args == 0 or len_args == 1


def parse_command(type_command, remaining_args):
    if type_command is CommandType.reg:
        args_for_command = get_dict_arg_for_registration(remaining_args)
    elif type_command is CommandType.login:
        args_for_command = get_dict_arg_for_login(remaining_args)
    elif type_command is CommandType.create:
        args_for_command = get_dict_arg_for_created(remaining_args)
    elif type_command is CommandType.reset:
        args_for_command = get_dict_arg_for_reset(remaining_args)
    elif type_command is CommandType.show:
        args_for_command = get_dict_arg_for_show(remaining_args)
    elif type_command is CommandType.status:
        args_for_command = get_dict_arg_for_status(remaining_args)
    elif type_command is CommandType.arch:
        args_for_command = get_dict_arg_for_arch(remaining_args)
    elif type_command is CommandType.access:
        args_for_command = get_dict_arg_for_access(remaining_args)
    elif type_command is CommandType.link:
        args_for_command = get_dict_arg_for_link(remaining_args)
    elif type_command is CommandType.modt:
        args_for_command = get_dict_arg_for_modt(remaining_args)
    elif type_command is CommandType.modm:
        args_for_command = get_dict_arg_for_modm(remaining_args)
    elif type_command is CommandType.add:
        args_for_command = get_dict_arg_for_added(remaining_args)
    elif type_command is CommandType.edit:
        args_for_command = get_dict_arf_for_edit_task(remaining_args)
    elif type_command is CommandType.editother:
        args_for_command = get_dict_arg_for_edit_other_task(remaining_args)
    elif type_command is CommandType.compl:
        args_for_command = get_dict_arg_for_compl_task(remaining_args)
    elif type_command is CommandType.remove:
        args_for_command = get_dict_arg_for_remove(remaining_args)
    elif type_command is CommandType.help:
        args_for_command = get_dict_arg_for_help(remaining_args)
    call_command(args_for_command)

def get_dict_arg_for_help(args):
    result = {'command': 'help'}
    result['type_command'] = args[0] if len(args) == 1 else 'all'
    return result

def get_dict_arg_for_added(args):
    return {'command': 'add', 'type': args[0]}

def get_dict_arg_for_registration(args):
    name = args[0]
    password = args[1]
    return {'command': 'reg', 'name': name, 'passwd': password}

def get_dict_arg_for_reset(args):
    result = {'command': 'reset', 'type': args[0]}
    result['tag'] = args[1] if args[0] == 'access' or args[0] == 'link' else None
    result['user'] = args[2] if len(args) == 3 else None
    return result

def get_dict_arg_for_remove(args):
    return {
        'command': 'remove',
        'type': args[0],
        'tag': args[1]
    }

def get_dict_arg_for_show(args):
    return {'command': 'show',
            'type': args[0],
            'param': args[1] if len(args) == 2 else None }

def get_dict_arg_for_login(args):
    return {'command': 'login', 'name': args[0]}

def command_for_added(args):
    return {'command': 'add', 'type': args[0]}

def get_dict_arg_for_created(args):
    return {'command': 'create', 'type': args[0]}

def get_dict_arg_for_status(args):
    return {'command': 'status', 'type': args[0]}

def get_dict_arg_for_arch(args):
    return {'command': 'arch', 'tag': args[0]}

def get_dict_arg_for_access(args):
    return {
        'command': 'access',
        'user': args[0],
        'tag': args[1],
        'type':args[2]
    }

def get_dict_arg_for_link(args):
    if len(args) == 2:
        return {
            'command': 'link',
            'master_tag': args[0],
            'type': args[1],
            'user': None,
            'sub_tag': None
        }
    if len(args) == 4:
        user = args[3]
    else:
        user = None
    return {
        'command': 'link',
        'master_tag': args[0],
        'sub_tag': args[1],
        'type': args[2],
        'user': user
    }

def get_dict_arg_for_modm(args):
    attribute = args[0]
    if 'date' in args:
        type_date = args[1]
        date = args[2]
        time = args[3] if len(args) == 4 else None
        return {
            'command': 'modm',
            'attr': attribute,
            'type': type_date,
            'date': date,
            'time': time
        }
    elif 'period' in args:
        return {
            'command': 'modm',
            'attr': attribute,
            'type': args[1],
            'interval': args[2]
        }
    return {
        'command': 'modm',
        'attr': attribute,
        'value': args[1]
    }

def get_dict_arg_for_modt(args):
    attribute = args[0]
    if 'date' in args:
        type_date = args[1]
        date = args[2]
        time = args[3] if len(args) == 4 else None
        return {
            'command': 'modt',
            'attr': attribute,
            'type': type_date,
            'date': date,
            'time': time
        }
    return {
        'command': 'modt',
        'attr': attribute,
        'value': args[1]
    }

def get_dict_arf_for_edit_task(args):
    result = {
        'command': 'edit',
        'tag': args[0],
        'attr': args[1],
        'value': args[2]
    }
    if len(args) == 4:
        result['value'] = [args[2], args[3]]
    return result

def get_dict_arg_for_edit_other_task(args):
    result = {
        'command': 'editother',
        'master': args[0],
        'tag': args[1],
        'field': args[2],
        'value': args[3]
    }
    if len(args) == 5:
        result['value'] = [args[3], args[4]]
    return result


def get_dict_arg_for_compl_task(args):
    return {
        'command': 'compl',
        'tag': args[0]
    }

def parser(args):
    try:
        type_command = CommandType[args[0]]
        remaining_args = args[1:]
    except KeyError:
        raise NonExistentArgumentError('Несуществующий аргумент: ' + args[0])
    if check_len_args(remaining_args, type_command):
        new_args = parse_command(type_command, remaining_args)
    else:
        raise InvalidNumberOfArgumentsError('Неверное кол-во параметров')
