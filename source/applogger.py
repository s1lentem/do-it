import logging

FORMAT = u'%(levelname)-8s [%(asctime)s]  %(message)s'
FILENAME = 'command.log'

def get_level_logger_from_str(level_str):
    if level_str == 'DEBUG':
        return logging.DEBUG
    elif level_str == 'NOTSET' or level_str is None:
        return logging.NOTSET


def get_log_for_app(path_to_file_for_log, level):
    if path_to_file_for_log is None:
        path_to_file_for_log = FILENAME
    level = get_level_logger_from_str(level)
    logging.basicConfig(filename=path_to_file_for_log, format=FORMAT,
                        level=level)
    return logging.getLogger(name='app')
