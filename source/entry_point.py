# !usr/bin/env python3

import sys

import source.terminal.parsargs as parser
from source.terminal.inindex import DuplicateIndexError
from tasktracklib.errors import (NonExistentArgumentError,
                        InvalidNumberOfArgumentsError,
                        DuplicateTagError)


LOGGER = get_log_for_app(None, 'INFO')

def main():
    try:
        args = sys.argv[1::]
        parser.parser(args)
    except NonExistentArgumentError:
        LOGGER.error('A non-existent command was given %s', args[0])
        print('Команды ' + args[0] + ' не существует')
    except InvalidNumberOfArgumentsError:
        LOGGER.error('The command "%s" must have a different number of parameters',
                     args[0])
        print('Команда ' + args[0] + ' должна иметь другое кол-во параметров')
    except DuplicateTagError:
        LOGGER.error('An attempt was made to add a task with an existing tag')
        print('Тэг, в задаче которой вы пытались добавить, уже занят')
    except DuplicateIndexError:
        LOGGER.error('Attempt to create an index that already exists')
        print('Индекс уже существует, перейдите к его редактированию')
    except Exception:
        print('Не верные параметры ввода')
        LOGGER.error('Invalid params %s', args[0])
