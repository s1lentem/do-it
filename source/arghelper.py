'''

The module describes all command-line arguments passed as a parameter
Contains only a set of constants

'''



DESCRIPTION_COMMAND_CREATE = '''
create

The create command "create" a temporary file, it takes one parameter
("task or" manager ") and, depending on it, creates a file for the task or task manager


$ di create task
'''

DESCRIPTION_COMMAND_EDIT = '''
edit

This is the command to edit the task, the first argument takes a tag
tasks, the second argument is the task field (tag, description, priority, status, date)
if the second argument was date, then the third argument is its type
(begin, end) otherwise, the value to which you want to change

$ di edit my_tag tag new_tag
'''

DESCRIPTION_COMMAND_ADD = '''
add

The command adds a task or task manager from the index to the repository.
As the second parameter, it takes an entity type that is necessary
save (task, manager)
'''

DESCRIPTION_COMMAND_ACCESS = '''
access

The command gives access to any user to your task.
The first argument is the name of the user you want to give access to.
The second parameter, the task tag, to which you want to give access.
The third is the type of access (write, readonly)
'''

DESCRIPTION_COMMAND_MODT = '''
modt

Command for adding / editing task index parameters.
The first argument takes the type of the field (tag, descript, priority, date)
The second - is the value. If there was a "date" then the second parameter is
its type (end, begin), the third - date, and the fourth one may be the time.

'''

DESCRIPTION_COMMAND_MODM = '''
modm

Command for adding / editing the parameters of the index of the manager.
The first argument takes the field type (tag, descript, datebegin, dateend,
period, priority)
the second is its value

'''

DESCRIPTION_COMMAND_COMPL = '''
compl

The command to mark the task as complete, takes as an argument
the task tag to be completed
'''


DESCRIPTION_COMMAND_STATUS = '''
status

Depending on the parameters: task, manager, user. Displays status information
task index, task manager and information about the current user, respectively
'''

DESCRIPTION_COMMAND_REG = '''
reg

The command to register the user
The first parameter takes the name (unique), the second - user's password
'''

DESCRIPTION_COMMAND_RESET = '''
reset

Commands for resetting indexes, and for resetting the connection between tasks
In the latter case,  second argument is given - the task tag
'''

DESCRIPTION_COMMAND_SHOW = '''
show

Displays your tasks
If you betray "all" - displays all tasks (including strangers, to which there is access)
task - displays the complete information about the task, the tag of which you pass by the second argument
arch - displays all tasks that are in the archive
status - displays all tasks with the status given by the second parameter
priority - displays all tasks with priority passed by the second parameter
project - build a task tree, the root task will be the tag that was passed by the second parameter
'''

DESCRIPTION_COMMAND_ARCH = '''
arch

puts the task with the tag passed as the second parameter to the archive
'''

DESCRIPTION_COMMAND_LOGIN = '''
login

Command to authorize the user. As applied, it takes a username
'''

DESCRIPTION_COMMAND_LINK = '''
link

The command for linking tasks depends on the
The first argument is the task you want to associate with the main
the second is the main task
the third is the dependency type (sub_task, paralell, stoped)
'''

DESCRIPTION_COMMAND_REMOVE = '''
remove

Command to task deletion, the tag of which is passed by the first parameter
'''

DESCRIPTION_COMMAND_EDITOTHER = '''
editother

Command for editing other people's tasks (to which you have access)
Similar to the edit command, only the first parameter accepts the user name.
The other parameters are shifted by one position
'''

ALL_DESCRIPTIONS = [DESCRIPTION_COMMAND_ADD, DESCRIPTION_COMMAND_ACCESS,
                    DESCRIPTION_COMMAND_MODT, DESCRIPTION_COMMAND_MODM,
                    DESCRIPTION_COMMAND_COMPL, DESCRIPTION_COMMAND_STATUS,
                    DESCRIPTION_COMMAND_REG, DESCRIPTION_COMMAND_CREATE,
                    DESCRIPTION_COMMAND_RESET, DESCRIPTION_COMMAND_EDIT,
                    DESCRIPTION_COMMAND_SHOW, DESCRIPTION_COMMAND_ARCH,
                    DESCRIPTION_COMMAND_LOGIN, DESCRIPTION_COMMAND_LINK,
                    DESCRIPTION_COMMAND_REMOVE, DESCRIPTION_COMMAND_EDITOTHER]


def get_description_for_command(command):
    if command == 'add':
        return DESCRIPTION_COMMAND_ADD
    elif command == 'access':
        return DESCRIPTION_COMMAND_ACCESS
    elif command == 'modt':
        return DESCRIPTION_COMMAND_MODT
    elif command == 'modm':
        return DESCRIPTION_COMMAND_MODM
    elif command == 'compl':
        return DESCRIPTION_COMMAND_COMPL
    elif command == 'status':
        return DESCRIPTION_COMMAND_STATUS
    elif command == 'reg':
        return DESCRIPTION_COMMAND_REG
    elif command == 'create':
        return DESCRIPTION_COMMAND_CREATE
    elif command == 'reset':
        return DESCRIPTION_COMMAND_RESET
    elif command == 'edit':
        return DESCRIPTION_COMMAND_EDIT
    elif command == 'show':
        return DESCRIPTION_COMMAND_SHOW
    elif command == 'arch':
        return DESCRIPTION_COMMAND_ARCH
    elif command == 'login':
        return DESCRIPTION_COMMAND_LOGIN
    elif command == 'link':
        return DESCRIPTION_COMMAND_LINK
    elif command == 'remove':
        return DESCRIPTION_COMMAND_REMOVE
    elif command == 'editother':
        return DESCRIPTION_COMMAND_EDITOTHER
    result = str()
    for description in ALL_DESCRIPTIONS:
        result += description + '\n' + '--'*30 + '\n'
    return result
