from setuptools import setup, find_packages
from os.path import dirname, join

packages = ['source', 'source.terminal']

setup(name='ditrack',
      version='1.0',
      description='task tracker',
      long_description=open(join(dirname(__file__), 'README_RU.md')).read(),
      packages=packages,
      entry_points={
          'console_scripts': ['ditrack = source.entry_point:main'] },
      install_requires=['enum34==1.1.6', 'termcolor==1.1.0',
                        'tasktracklib==1.0'])
