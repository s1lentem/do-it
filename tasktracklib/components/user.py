from enum import Enum

class AccessType(Enum):
    readonly = 0
    write = 1

class AccessRights:
    def __init__(self, master, user, tag, acc_type):
        self.master = master
        self.user = user
        self.tag = tag
        if isinstance(acc_type, AccessType):
            self.type = acc_type
        elif isinstance(acc_type, str):
            self.type = AccessType[acc_type]
        elif isinstance(acc_type, int):
            self.type = AccessType(acc_type)
        else:
            raise NotImplemented('Invalid param')

    def __eq__(self, other):
        try:
            return (self.master == other.master and self.user == other.user and
                    self.tag == other.tag)
        except AttributeError:
            return False


class UserInfo:
    def __init__(self, name, password, messages=None):
        self.name = name
        self.password = password
        if messages is None:
            self.messages = list()
        else:
            self.messages = messages

    def leave_message(self, message):
        self.messages.append(message)

    def __str__(self):
        return self.name
