import datetime
from enum import Enum


class DependenceType(Enum):
    parallel = 0
    stoped = 1
    subtask = 2

class Priority(Enum):
    low = 0
    medium = 1
    height = 2
    critical = 3

class StatusType(Enum):
    not_started = 0
    durning = 1
    completed = 2
    failed = 3

class AccessType(Enum):
    readonly = 0
    write = 1

class Timing:
    def __init__(self, date_end=None, date_begin=None):
        if date_begin is None:
            self.begin = datetime.datetime.now()
        else:
            if not (isinstance(date_end, datetime.datetime) and
                    isinstance(date_begin, datetime.datetime)):
                raise ValueError('Argument not datetime')
            self.begin = date_begin
        self.end = date_end
        self.change = None
        self.create = datetime.datetime.now()

    def __str__(self):
        if not (self.end is None):
            return self.end.strftime('%d.%m.%Y %H:%M')
        return 'Unlimited'

class Dependence:
    def __init__(self, dep_task_tag, dep_type, master_task=None):
        self.task_tag = dep_task_tag
        self.type = dep_type
        self.master = master_task

class Status:
    def __init__(self, priority=Priority.low, state=StatusType.not_started):
        self.state = state
        if isinstance(priority, str):
            self.priority = Priority[priority]
        else:
            self.priority = priority
