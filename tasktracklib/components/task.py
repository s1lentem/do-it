import datetime as dt

from tasktracklib.dateparse import get_next_date
from tasktracklib.components.taskcomp import *

class SuperfluousDependece(Exception):
    pass

class Task:
    def __init__(self, timing, description, tag, comments=list(),
                 status=Status(), dependence=None, types_notification=''):
        '''A class that stores all the information about the task

        Timing stores the start and end dates of the task, which can
        edit the user and the date of the change and task creation, to
        which the user does not have access to

        description stores a string description of tasks

        tag stores a string unique identifier for the task

        comments stores a list of comments to the task

        status stores object of type Status

        dependencies stores the dependency of a task in the Dependence type

        '''


        self.timing = timing
        self.description = description
        self.comments = comments
        self.status = status
        self.tag = tag
        self.dependencies = dependence
        self.types_notification = types_notification

    def __str__(self):
        return self.description

    def link_to(self, tag, type, user=None):
        '''The method of linking the problem to another task'''
        if isinstance(type, str):
            type = DependenceType[type]
        dependece = Dependence(tag, type, user)
        if self.dependencies is not None:
            raise SuperfluousDependece(self.dependencies.type)
        self.dependencies = dependece

    def reset_link(self):
        self.dependencies = None


class GroupTasks:
    '''The class container for tasks, storing from a list, and also has a name'''
    def __init__(self, name, tasks=None):
        self.name = name
        if tasks is None:
            self.tasks = []
        else:
            self.tasks = tasks

    def add_task(self, task):
        self.tasks.append(task)

    def remove_task_by_tag(self, tag):
        tags_all_tasks = [task.tag for task in self.tasks]
        if not (tag in tags_all_tasks):
            raise Exception('Задачи с таким тегом не существует')
        index_tag = tags_all_tasks.index(tag)
        return self.tasks.pop(index_tag)

    def remove_task(self, task):
        self.tasks.remove(task)

    def search_task(self, tag):
        for task in self.tasks:
            if tag == task.tag:
                return task

    def clear(self):
        self.tasks.clear()


class ManagerTask:
    '''A class that allows you to generate tasks for certain priviledges
    tag a unique identifier for the instance

    datebegin and dateend start dates about the end of the task generation period

    description a string description of the generated tasks

    last_date the date of the last created task

    period string representation of the task generation interval, consisting of
    from any number and one word (day, week, mount) separated by a space

    '''

    def __init__(self, date_begin, date_end, description, tag, period,
                 last_date=None):
        self.tag = tag
        self.date_begin = date_begin
        self.date_end = date_end
        self.description = description
        self.period = period
        self.last_date = last_date


    def period_is_complete(self):
        '''Informs about the completion of the period of generating tasks'''
        if self.last_date is not None:
            return self.last_date > self.date_end
        return False

    def create_task(self):
        if self.last_date is None:
            current_date = self.date_begin
            self.last_date = current_date
        else:
            current_date = get_next_date(self.last_date, self.period)
            self.last_date = current_date
        return Task(Timing(current_date), self.description, self.tag)
