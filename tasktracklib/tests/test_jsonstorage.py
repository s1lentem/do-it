import mock
import unittest
import json

from datetime import datetime, timedelta

import tasktracklib.storages.jsonstorage.jsonstorage as jstorage
import tasktracklib.storages.jsonstorage.filesys as filesys
import tasktracklib.storages.jsonstorage.JSONparse as JSONparse

from tasktracklib.components.task import Task, Timing
from tasktracklib.components.user import AccessRights, AccessType
from tasktracklib.errors import NotSuitableDictionaryError

class TestStorage(unittest.TestCase):

    def test_successful_task_serialization(self):
        date = datetime.now() + timedelta(days=13)
        date = date - timedelta(microseconds=date.microsecond,
                                seconds=date.second)
        task = Task(Timing(date), 'test_message', 'test')

        task_in_json = JSONparse.get_JSON_from_object(task)

        task_from_json = JSONparse.get_task(json.loads(task_in_json))

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.tag, task_from_json.tag)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.description, task_from_json.description)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.status.state, task_from_json.status.state)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.status.priority, task_from_json.status.priority)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.dependencies, task_from_json.dependencies)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.timing.end, task_from_json.timing.end)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.timing.change, task_from_json.timing.change)

        with self.subTest(task_from_json=task_from_json):
            self.assertEqual(task.types_notification,  task_from_json.types_notification)


    def test_unsuccessful_task_serialization(self):
        random_dictionary = {
            'tag': 'tag',
            'description': 'test',
            'cat': 1,
            'dog': 'hotdog',
            'me': 'Deleted from the University due to ISP'}
        with self.assertRaises(NotSuitableDictionaryError):
            result = JSONparse.get_task(random_dictionary)


    def test_serialization_access_right_serialization(self):
        access = AccessRights('test_master', 'test_user', 'test_tag',
                              AccessType.readonly)
        access_in_json = JSONparse.get_JSON_from_object(access)
        access_from_json = JSONparse.get_access_right([json.loads(access_in_json)])[0]

        with self.subTest(access_from_json=access_from_json):
            self.assertEqual(access.master, access_from_json.master)

        with self.subTest(access_from_json=access_from_json):
            self.assertEqual(access.tag, access_from_json.tag)

        with self.subTest(access_from_json=access_from_json):
            self.assertEqual(access.user, access_from_json.user)

        with self.subTest(access_from_json=access_from_json):
            self.assertEqual(access.type, access_from_json.type)
