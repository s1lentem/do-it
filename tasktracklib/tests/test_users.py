import unittest
import mock

from tasktracklib.storages.storage_interface import (ITaskStorage, IUserStorage,
                                                     ITaskManagerStorage)
from tasktracklib.storagemanager import DataStorageManager
from tasktracklib.errors import DuplicateUserNameError


class TestUsers(unittest.TestCase):

    @mock.patch.object(IUserStorage, 'add_new_user', return_value=None)
    @mock.patch.object(IUserStorage, 'is_duplicate_user_name', return_value=True)
    def test_raise_duplicate_user_name_errors(self, is_duplicate_user_name_mock,
                                              add_new_user_mock):
        test_data_storage = DataStorageManager(ITaskStorage(), IUserStorage(),
                                               ITaskManagerStorage(), 'test')

        with self.assertRaises(DuplicateUserNameError):
            test_data_storage.register_user('test', 'test')
