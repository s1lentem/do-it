import unittest
import mock

import tasktracklib.components.user as user
import tasktracklib.components.task as task

from tasktracklib.storagemanager import DataStorageManager
from tasktracklib.storages.storage_interface import (ITaskStorage, IUserStorage,
                                                     ITaskManagerStorage)
from tasktracklib.logger import get_lib_log
from logging import Logger

class TestAccess(unittest.TestCase):
    def setUp(self):
        self.user = user.UserInfo('test', 'test')
        self.log_patch = mock.patch('tasktracklib.logger.get_lib_log')
        self.log_patch.start()
        self.logging_mock = mock.patch('logging.Logger')
        self.logging_mock.start()

    def test_notification(self):
        str_in_object = 'tasktracklib.storages.storage_interface.IUserStorage.get_user'
        test_data_storage = DataStorageManager(ITaskStorage(),
                                               IUserStorage(),
                                               ITaskManagerStorage(),
                                               'test')
        with mock.patch(str_in_object) as send_notification_mock:
            send_notification_mock.return_value = self.user

            test_data_storage.send_notification(['test'], 'test message')

            self.assertEqual('test message', self.user.messages[-1])


    def test_link_task_sub_tag(self):
        sub_tag = 'test_tag'
        master_task = task.Task(task.Timing(), 'test', 'test')
        task_storage_mock = ITaskStorage()
        test_data_storage = DataStorageManager(task_storage_mock, IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        task_storage_mock.get_task_by_tag = lambda tag: (master_task, None)
        task_storage_mock.save_state = lambda : None

        test_data_storage.link_task_with(None, 'test', sub_tag,
                                         task.DependenceType.parallel)

        with self.subTest(master_task=master_task):
            self.assertEqual(master_task.dependencies.task_tag, sub_tag)
        with self.subTest(master_task=master_task):
            self.assertEqual(master_task.dependencies.type,
                             task.DependenceType.parallel)


    @mock.patch('logging.Logger')
    @mock.patch.object(DataStorageManager, 'send_notification', return_value=None)
    def test_gived_access_user(self, send_notification_mock, logger_mock):
        all_access_rigth = list()
        user_storage_mock = IUserStorage()
        test_data_storage = DataStorageManager(ITaskStorage(), user_storage_mock,
                                               ITaskManagerStorage(),
                                               user.UserInfo('test', 'temp'))
        # print(type(test_data_storage.current_user))
        user_storage_mock.save_state = lambda : None
        user_storage_mock.give_access_right_to = (lambda access_right_info :
                                                  all_access_rigth.append(access_right_info))
        user_storage_mock.get_user = lambda : self.user

        test_data_storage.give_access_to('test', 'test_name', 'readonly')

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertEqual(all_access_rigth[-1].tag, 'test')

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertEqual(all_access_rigth[-1].user, 'test_name')

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertEqual(all_access_rigth[-1].type.name,
                             task.AccessType.readonly.name)

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertEqual(all_access_rigth[-1].master.name, self.user.name)

    @mock.patch('logging.Logger')
    @mock.patch.object(DataStorageManager, 'send_notification', return_value=None)
    def test_archiving_task(self, send_notification_mock, logger_mock):
        task_storage_mock = ITaskStorage()
        user_storage_mock = IUserStorage()
        test_task = task.Task(task.Timing(), 'test', 'test')
        test_group = task.GroupTasks('test', [test_task])
        test_archiv = task.GroupTasks('test', [])
        task_storage_mock.get_task_by_tag = lambda tag: (test_task, test_group)
        task_storage_mock.get_archived_tasks = lambda : test_archiv
        task_storage_mock.get_all_tasks = lambda : list()
        task_storage_mock.save_state = lambda : None
        user_storage_mock.get_users_with_access_to = lambda tag: []
        user_storage_mock.save_state = lambda : None
        user_storage_mock.remove_access_right = lambda tag: None
        test_data_storage = DataStorageManager(task_storage_mock, user_storage_mock,
                                               ITaskManagerStorage(), 'test')

        DataStorageManager.put_task_in_archive(test_data_storage, 'test')

        with self.subTest(test_archiv=test_archiv):
            self.assertEqual(test_archiv.tasks[0], test_task)

        with self.subTest(test_group=test_group):
            self.assertEqual(len(test_group.tasks), 0)



    @mock.patch.object(DataStorageManager, 'send_notification', return_value=None)
    @mock.patch.object(IUserStorage, 'get_all_access_manager')
    def test_getter_away_access_from(self, get_all_access_manager_mock,
                                     send_notification_mock):
        test_data_storage = DataStorageManager(ITaskStorage(), IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        test_data_storage.user_storage.save_state = lambda : None
        first_access_right = user.AccessRights('test_master', 'me', 'test_tag',
                                               user.AccessType.readonly)
        second_access_right = user.AccessRights('test_master', 'other_user',
                                                'test_tag', user.AccessType.write)
        third_access_right = user.AccessRights('me', 'other_user', 'my_tag',
                                               user.AccessType.write)
        all_access_rigth = [first_access_right, second_access_right,
                            third_access_right]
        get_all_access_manager_mock.return_value = all_access_rigth

        test_data_storage.get_away_access_from('test_tag', 'me')

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertEqual(len(all_access_rigth), 2)

        with self.subTest(all_access_rigth=all_access_rigth):
            self.assertTrue(first_access_right)


    def tearDown(self):
        self.log_patch.stop()
        self.logging_mock.stop()

if __name__ == '__main__':
    unittest.main()
