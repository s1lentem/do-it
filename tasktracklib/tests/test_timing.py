import unittest
import mock
import datetime

import tasktracklib.components.task as task
from tasktracklib.storagemanager import DataStorageManager
from tasktracklib.storages.storage_interface import (ITaskStorage, IUserStorage,
                                                     ITaskManagerStorage)

class TestTimingTask(unittest.TestCase):

    @mock.patch.object(ITaskStorage, 'get_all_tasks')
    def test_dedline_task(self, get_all_tasks_mock):
        current_date = datetime.datetime.now()
        test_task = task.Task(task.Timing(current_date -
                                          datetime.timedelta(days=10)), 'test',
                              'test')
        task_storage_mock = ITaskStorage()
        test_data_storage = DataStorageManager(task_storage_mock, IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        task_storage_mock.save_state = lambda : None
        get_all_tasks_mock.return_value = [test_task]

        result = test_data_storage.check_deadline_for_each_task()

        self.assertEqual(result[0], test_task)


    def test_next_date_manager(self):
        date_begin = datetime.datetime(2018, 6, 3)
        date_end = datetime.datetime(2018, 7, 4)
        manager = task.ManagerTask(date_begin, date_end, 'test', 'test',
                                   'week 1')
        with self.subTest(manager=manager):
            temp_task = manager.create_task()

            self.assertEqual(temp_task.timing.end, date_begin)

        with self.subTest(manager=manager):
            temp_task = manager.create_task()

            self.assertEqual(temp_task.timing.end,
                             date_begin + datetime.timedelta(days=7))

        with self.subTest(manager=manager):
            '''10 + 7 = 17 + 7 = 24 + 7 = 1 + 7 = 7 > 4 -> 4 итерации'''
            iteration = 0
            while not manager.period_is_complete():
                manager.create_task()
                iteration += 1

        self.assertEqual(iteration, 4)
