import unittest
import mock

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

import tasktracklib.dateparse as dateparse

class TestDateParser(unittest.TestCase):


    def test_checking_notification_date(self):
        current_date = datetime.now()
        test_date = current_date + timedelta(days=1)
        types_notifications = '1 days'

        result = dateparse.checking_notification_date(test_date,
                                                      types_notifications)
        with self.subTest(result=result):
            self.assertEqual(result[0], True)

        with self.subTest(result=result):
            self.assertEqual(result[1], '')


    def test_checking_more_notification_dates(self):
        current_date = datetime.now()
        test_date = current_date + timedelta(weeks=1)
        types_notifications = '1 week|1 day'

        result = dateparse.checking_notification_date(test_date,
                                                      types_notifications)
        with self.subTest(result=result):
            self.assertEqual(result[0], True)

        with self.subTest(result=result):
            self.assertEqual(result[1], '1 day')


    def test_getter_next_date(self):
        current_date = datetime.now()

        with self.subTest(current_date=current_date):
            period = 'day 2'

            new_date = dateparse.get_next_date(current_date, period)

            self.assertEqual(new_date, current_date + timedelta(days=2))

        with self.subTest(current_date=current_date):
            period = 'week 4'

            new_date = dateparse.get_next_date(current_date, period)

            self.assertEqual(new_date, current_date + timedelta(weeks=4))

        with self.subTest(current_date=current_date):
            period = 'month 1'

            new_date = dateparse.get_next_date(current_date, period)

            self.assertEqual(new_date, current_date + relativedelta(months=+6))


    def test_date_equality_for_reverse_str(self):
        first_test_str = '12:02:2019'
        second_test_str = '2019:02:12'

        first_result = dateparse.get_datetime(first_test_str)
        second_result = dateparse.get_datetime(second_test_str)

        self.assertEqual(first_result, second_result)


    def test_getter_datetime_without_time(self):
        test_str = '14:02:1999'

        result = dateparse.get_datetime(test_str)

        self.assertEqual(result, datetime(1999, 2, 14))


    def test_getter_next_date_with_time(self):
        test_str = ['14:02:1999','14:01']

        result = dateparse.get_datetime(test_str)

        self.assertEqual(result, datetime(1999, 2, 14, 14, 1))
