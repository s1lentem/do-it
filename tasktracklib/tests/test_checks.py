import unittest
import mock
import datetime

import tasktracklib.components.task as task
import tasktracklib.components.taskcomp as tcomp

from tasktracklib.components.user import AccessRights, AccessType
from tasktracklib.storages.storage_interface import (ITaskStorage, IUserStorage,
                                                     ITaskManagerStorage)
from tasktracklib.storagemanager import DataStorageManager
from tasktracklib.errors import EndTaskGeneration

class TestCheckFuncs(unittest.TestCase):
    def test_access_right_for_read(self):
        user_storage_mock = IUserStorage()
        first_access_right = AccessRights('test', 'test_name', 'test_tag',
                                          AccessType.readonly)
        second_access_right = AccessRights('test', 'fake_name', 'test_tag',
                                           AccessType.write)
        test_data_storage = DataStorageManager(ITaskStorage(), user_storage_mock,
                                               ITaskManagerStorage(), 'test')
        user_storage_mock.get_current_user_name = lambda : 'test_name'
        user_storage_mock.get_access_right_for_master_with_tag = (lambda tag, master:
                                                                  [first_access_right,
                                                                   second_access_right])

        result = test_data_storage.check_access_right_for_read('test_name', 'test')

        self.assertTrue(result)


    def test_not_access_right_for_read(self):
        user_storage_mock = IUserStorage()
        first_access_right = AccessRights('test', 'test_other_name', 'test_tag',
                                          AccessType.readonly)
        second_access_right = AccessRights('test', 'fake_name', 'test_tag',
                                           AccessType.write)
        test_data_storage = DataStorageManager(ITaskStorage(), user_storage_mock,
                                               ITaskManagerStorage(), 'test')
        user_storage_mock.get_current_user_name = lambda : 'test_name'
        user_storage_mock.get_access_right_for_master_with_tag = (lambda tag, master:
                                                                  [first_access_right,
                                                                   second_access_right])

        result = test_data_storage.check_access_right_for_read('test_name', 'test')

        self.assertFalse(result)


    @mock.patch.object(ITaskStorage, 'get_task_by_tag')
    def test_check_dependece_for_change_status_false(self, get_task_by_tag_mock):
        test_data_storage = DataStorageManager(ITaskStorage(), IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        first_task = task.Task(task.Timing(), 'test_tag', 'test', list(),
                         tcomp.Status(state=task.StatusType.not_started),
                         dependence=task.Dependence('mastet_tag',
                                                    task.DependenceType.parallel,
                                                    None))
        second_task = task.Task(task.Timing(), 'master_tag', 'test', list(),
                                tcomp.Status(state=task.StatusType.durning),
                                None)
        get_task_by_tag_mock.return_value = (second_task, None)
        value = 'completed'

        result = test_data_storage.check_dependece_for_change_status(first_task,
                                                                     value)

        self.assertFalse(result)


    @mock.patch.object(ITaskStorage, 'get_task_by_tag')
    def test_check_dependece_for_change_status_true(self, get_task_by_tag_mock):
        test_data_storage = DataStorageManager(ITaskStorage(), IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        first_task = task.Task(task.Timing(), 'test_tag', 'test', list(),
                         tcomp.Status(state=task.StatusType.not_started),
                         dependence=task.Dependence('mastet_tag',
                                                    task.DependenceType.parallel,
                                                    None))
        second_task = task.Task(task.Timing(), 'master_tag', 'test', list(),
                                tcomp.Status(state=task.StatusType.completed),
                                None)
        get_task_by_tag_mock.return_value = (second_task, None)
        value = 'completed'

        result = test_data_storage.check_dependece_for_change_status(first_task,
                                                                     value)

        self.assertTrue(result)


    @mock.patch.object(DataStorageManager, 'add_task', return_value=None)
    @mock.patch.object(ITaskStorage, 'get_task_by_tag', return_value=None)
    @mock.patch.object(ITaskManagerStorage, 'get_all_task_managers')
    def test_checking_tasks_from_manageres(self, get_all_task_managers_mock,
                                           get_task_by_tag_mock, add_task_mock):
        test_data_storage = DataStorageManager(ITaskStorage(), IUserStorage(),
                                               ITaskManagerStorage(), 'test')
        test_data_storage.task_manager_storage.save_state = lambda : None
        current_date = datetime.datetime.now()
        datebegin = current_date - datetime.timedelta(weeks=1)
        dateend = current_date + datetime.timedelta(weeks=1)
        tag = 'test_tag'
        description = 'test'
        period = 'week 1'
        manager_tasks = task.ManagerTask(datebegin, dateend, description, tag,
                                         period)
        get_all_task_managers_mock.return_value = [manager_tasks]

        test_data_storage.check_tasks_from_managers()

        with self.subTest(manager_tasks=manager_tasks):
            self.assertEqual(manager_tasks.last_date, manager_tasks.date_begin)

        with self.subTest(manager_tasks=manager_tasks):
            with self.assertRaises(EndTaskGeneration):
                for i in range(4):
                    test_data_storage.check_tasks_from_managers()
