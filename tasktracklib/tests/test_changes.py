import unittest
import mock
import datetime

import tasktracklib.storages.storage_interface as istorage
import tasktracklib.components.task

from tasktracklib.components.taskcomp import Timing
from tasktracklib.storagemanager import DataStorageManager
# import source.func.taskfunc as tf

class TestEditTaskFuncs(unittest.TestCase):
    def setUp(self):
        self.delta = datetime.timedelta(microseconds=2000)
        self.task = tasktracklib.components.task.Task(Timing(), 'test', 'test')
        self.data_manager = DataStorageManager(istorage.ITaskStorage(),
                                               istorage.IUserStorage(),
                                               istorage.ITaskManagerStorage(),
                                               'test')



    def test_created_date_task(self):
        current_date_time = datetime.datetime.now()

        self.assertLess(current_date_time - self.task.timing.create, self.delta)



    @mock.patch.object(DataStorageManager, 'update_tag_in_access_right',
                       return_value=None)
    def test_date_change_task(self, update_tag_in_access_right_mock):
        current_date_time = datetime.datetime.now()
        self.data_manager.task_storage.get_task_by_tag = lambda tag: self.task

        self.data_manager.edit_field_task(self.task, 'test', 'tag')

        self.assertLess(current_date_time - self.task.timing.change, self.delta)



    @mock.patch.object(DataStorageManager, 'update_tag_in_access_right',
                       return_value=None)
    def test_edit_tag_task(self, update_tag_in_access_right_mock):
        self.data_manager.task_storage.get_task_by_tag = lambda tag: self.task

        self.data_manager.edit_field_task(self.task, 'test', 'tag')

        self.assertEqual(self.task.tag, 'test')



    def test_edit_default_date_created(self):
        date_now = datetime.datetime.now()

        self.assertLess(date_now - self.task.timing.begin, self.delta)



    @mock.patch.object(istorage.ITaskStorage, 'save_state', return_value=None)
    @mock.patch.object(istorage.ITaskStorage, 'get_archived_tasks')
    def test_reseted_archiv(self, get_archived_tasks_mock, save_state_mock):
        test_data_storage = DataStorageManager(istorage.ITaskStorage(),
                                               istorage.IUserStorage(),
                                               istorage.ITaskManagerStorage(),
                                               'test')
        archiv = ['task1', 'task2', 'task3']
        get_archived_tasks_mock.return_value = archiv
        expected_result = list()

        test_data_storage.reset_archiv()

        self.assertEqual(archiv, expected_result)




if __name__ == '__main__':
    unittest.main()
