import json
import datetime

from enum import Enum

from tasktracklib.components.user import UserInfo, AccessRights
from tasktracklib.components.task import (Timing, Priority, Dependence,
                                DependenceType, Status, Task,
                                ManagerTask, StatusType, GroupTasks)
from tasktracklib.dateparse import get_datetime
from tasktracklib.errors import NotSuitableDictionaryError

ATTRIBUTE_PERSONAL_INFO = ['name', 'password', 'messages']
ATTRIBUTE_AVAILABLE_TASK = ['access_rights', 'user']
ATTRIBUTE_TASK = ['timing', 'description', 'comments', 'status', 'tag',
                  'dependencies']
ATTRIBUTE_TIMING = ['change', 'end', 'begin', 'create']
ATTRIBUTE_STATUS = ['state', 'priority']
ATTRIBUTE_DEPENDECE = ['task_tag', 'type', 'master']
ATTRIBUTE_GROUP = ['name', 'tasks']
ATTRIBUTE_FOR_MANGER = ['tag', 'date_begin', 'date_end', 'last_date', 'period',
                    'description']
ATRIBUTE_ACCESS_RIGHTS = ['user', 'tag', 'type', 'master']



def encode_task(obj):
    '''
    The decoder required for parsing in json is "not serializable types"

    '''
    if isinstance(obj, datetime.datetime):
        return '{}-{}-{} {}:{}'.format(obj.year, obj.month, obj.day,
                                       obj.hour, obj.minute)
    if isinstance(obj, Enum):
        return obj.name
    return obj.__dict__

def is_contains_attribute(dictionary, list_attribute):
    '''
    Checks for the presence of the necessary keys in the dictionary
    to correctly restore the object

    Accepts a dictionary that needs to be parsed and a list of all required keys
    '''
    for attribute in list_attribute:
        if not (attribute in dictionary):
            return False
    return True

def get_personal_info(dictionary):
    '''
    To parse a dictionary into an object of type "PersonalInfo"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_PERSONAL_INFO):
        raise NotSuitableDictionaryError(dictionary)
    return UserInfo(dictionary['name'], dictionary['password'],
                    dictionary['messages'])

def get_timing(dictionary):
    '''
    To parse a dictionary into an object of type "Timing"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_TIMING):
        raise NotSuitableDictionaryError(dictionary)
    change = (get_datetime(dictionary['change'].split(' ')) if
              not (dictionary['change'] is None) else None)
    # print(dictionary['end'].split(' '))
    end = (get_datetime(dictionary['end'].split(' ')) if
           not (dictionary['end'] is None) else None)
    begin = get_datetime(dictionary['begin'])
    create = get_datetime(dictionary['create'])
    timing = Timing(end, begin)
    timing.change = change
    timing.create = create
    return timing

def get_dependence(dictionary):
    '''
    To parse a dictionary into an object of type "Dependence"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if dictionary is None:
        return None
    if not is_contains_attribute(dictionary, ATTRIBUTE_DEPENDECE):
        raise NotSuitableDictionaryError(dictionary)
    tag = dictionary['task_tag']
    type_dep = DependenceType[dictionary['type']]
    master = dictionary['master']
    return Dependence(tag, type_dep, master)

def get_status(dictionary):
    '''
    To parse a dictionary into an object of type "Status"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_STATUS):
        raise NotSuitableDictionaryError(dictionary)
    state = StatusType[dictionary['state']]
    priority = Priority[dictionary['priority']]
    return Status(priority, state)

def get_task(dictionary):
    '''
    To parse a dictionary into an object of type "Task"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_TASK):
        raise NotSuitableDictionaryError(dictionary)
    tag = dictionary['tag']
    timing = get_timing(dictionary['timing'])
    descript = dictionary['description']
    comments = dictionary['comments']
    status = get_status(dictionary['status'])
    dependence = get_dependence(dictionary['dependencies'])
    notifications = (dictionary['types_notification'] if
                     'types_notification' in dictionary else '')
    return Task(timing, descript, tag, comments, status, dependence, notifications)

def get_group_task(dictionary):
    '''
    To parse a dictionary into an object of type "GroupTasks"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_GROUP):
        raise NotSuitableDictionaryError(dictionary)
    name = dictionary['name']
    tasks = list()
    for dict_task in dictionary['tasks']:
        task = get_task(dict_task)
        tasks.append(task)
    return GroupTasks(name, tasks)

def get_all_groups(dictionary_list):
    '''
    To parse a dictionary into an list objects of type "GroupTasks"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    all_groups = list()
    for dictionary_group in dictionary_list:
        group = get_group_task(dictionary_group)
        all_groups.append(group)
    return all_groups

def get_manager_task(dictionary):
    '''
    To parse a dictionary into an object of type "TaskManager"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    if not is_contains_attribute(dictionary, ATTRIBUTE_FOR_MANGER):
        raise NotSuitableDictionaryError(dictionary)
    if isinstance(dictionary['date_begin'], str):
        date_begin = get_datetime(dictionary['date_begin'])
    if isinstance(dictionary['date_end'], str):
        date_end = get_datetime(dictionary['date_end'])
    else:
        date_end = dictionary['date_end']
    if dictionary['last_date'] is None:
        last_date = None
    else:
        last_date = get_datetime(dictionary['last_date'])
    return ManagerTask(date_begin, date_end, dictionary['description'],
                       dictionary['tag'], dictionary['period'], last_date)

def get_access_right(dictionary):
    '''
    To parse a dictionary into an object of type "AccessRights"
    As an argument, it takes a dictionary, where the key is an attribute of the class

    '''
    result = list()
    for item in dictionary:
        if not is_contains_attribute(item, ATRIBUTE_ACCESS_RIGHTS):
            raise NotSuitableDictionaryError(dictionary)
        temp = AccessRights(item['master'], item['user'], item['tag'],
                            item['type'])
        result.append(temp)
    return result

def get_JSON_from_object(obj):
    '''Converts an object to a json format string'''
    return json.dumps(obj, default=encode_task, sort_keys=True, indent=4)
