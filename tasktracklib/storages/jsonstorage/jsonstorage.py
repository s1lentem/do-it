# import source.terminal.storage.filesys as fs
# import source.terminal.storage.JSONparse as jp

import tasktracklib.storages.jsonstorage.filesys as fs
import tasktracklib.storages.jsonstorage.JSONparse as jp

from tasktracklib.storages.storage_interface import (ITaskStorage,
                                                     IUserStorage,
                                                     ITaskManagerStorage)
from tasktracklib.components.user import AccessRights


class JSONTaskStorage(ITaskStorage):
    def __init__(self, user_name    ):
        self.user_name = user_name
        self.all_groups = None
        self.other_group = None
        self.archiv_tasks = None

    def get_all_groups(self):
        if self.all_groups is not None:
            return self.all_groups
        path_for_user = fs.get_path_for_users()
        dict_all_groups = fs.get_dict_info(path_for_user, self.user_name,
                                           'tasks')
        if self.all_groups is None:
            all_groups = jp.get_all_groups(dict_all_groups)
            self.all_groups = all_groups
        return all_groups

    def get_group(self, group_name):
        if self.all_groups is None:
            JSONTaskStorage.get_all_groups(self)
        for group in self.all_groups:
            if group.name == group_name:
                return group

    def get_task_by_tag(self, tag, user=None):
        if user is None:
            if self.all_groups is None:
                JSONTaskStorage.get_all_groups(self)
            for group in self.all_groups:
                for task in group.tasks:
                    if task.tag == tag:
                        return (task, group)
        else:
            path_for_user = fs.get_path_for_users()
            dict_all_groups = fs.get_dict_info(path_for_user, user,
                                               'tasks')
            all_groups = jp.get_all_groups(dict_all_groups)
            self.other_group = (all_groups, user)
            for group in all_groups:
                for task in group.tasks:
                    if task.tag == tag:
                        return (task, group)

    def get_all_tasks(self):
        if self.all_groups is None:
            JSONTaskStorage.get_all_groups(self)

        all_tasks = list()
        for group in self.all_groups:
            all_tasks = all_tasks + group.tasks
        return all_tasks

    def get_archived_tasks(self):
        path_to_all_users = fs.get_path_for_users()
        dict_archive = fs.get_dict_info(path_to_all_users, self.user_name,
                                        'archiv')
        archiv = jp.get_group_task(dict_archive)
        self.archiv_tasks = archiv
        return archiv

    def tag_is_not_busy(self, tag):
        if self.all_groups is None:
            JSONTaskStorage.get_all_groups(self)
        for group in self.all_groups:
            for task in group.tasks:
                if task.tag == tag:
                    return False
        return True

    def save_state(self):
        out = False
        path_to_all_users = fs.get_path_for_users()
        if self.all_groups is not None:
            temp_json = jp.get_JSON_from_object(self.all_groups)
            fs.save_dict_info(path_to_all_users, self.user_name, 'tasks',
                              temp_json)
            out = True
        if self.archiv_tasks is not None:
            temp_json = jp.get_JSON_from_object(self.archiv_tasks)
            fs.save_dict_info(path_to_all_users, self.user_name, 'archiv',
                              temp_json)
            out = True
        if self.other_group is not None:
            temp_json = jp.get_JSON_from_object(self.other_group[0])
            fs.save_dict_info(path_to_all_users, self.other_group[1], 'tasks',
                              temp_json)
        return out



class JSONUserStorage(IUserStorage):
    def __init__(self, user):
        self.all_access_right = None
        self.users = list()
        self.user = user

    def get_all_access_manager(self):
        path_to_all_users = fs.get_path_for_users()
        temp_dict = fs.get_access_file(path_to_all_users)
        list_access_rights = jp.get_access_right(temp_dict)
        self.all_access_right = list_access_rights
        return self.all_access_right

    def get_access_right_for_master_with_tag(self, tag, master=None):
        if master is None:
            master = self.user
        list_access_rights = JSONUserStorage.get_all_access_manager(self)
        return [item for item in list_access_rights if
                item.master == master and item.tag == tag]

    def get_users_with_access_to(self, tag, task_master=None):
        if task_master is None:
            task_master = self.user
        list_access_rights = JSONUserStorage.get_all_access_manager(self)
        all_user_name = [item.user for item in list_access_rights if
                         item.master == task_master and item.tag == tag]
        for user in all_user_name:
            JSONUserStorage.get_user(self, user)
        return self.users

    def get_tasks_are_available_for(self, user_name):
        all_access_right = JSONUserStorage.get_all_access_manager(self)
        return [item for item in all_access_right if item.user == user_name]

    def give_access_right_to(self, access_right_info):
        list_access_rights = JSONUserStorage.get_all_access_manager(self)
        if access_right_info in list_access_rights:
            index = list_access_rights.index(access_right_info)
            list_access_rights[index].type = access_right_info.type
        else:
            list_access_rights.append(access_right_info)

    def remove_access_right(self, task_tag, user=None):
        list_access_rights = JSONUserStorage.get_all_access_manager(self)
        if user is None:
            removed_access = [item for item in list_access_rights if
                              item.tag == task_tag and item.master == self.user]

        else:
            removed_access = [item for item in list_access_rights if
                              item.tag == task_tag and item.master == self.user and
                              item.user == user]
        for access in removed_access:
            list_access_rights.remove(access)

    def get_user(self, user_name=None):
        path_to_all_users = fs.get_path_for_users()
        if user_name is None:
            user_name = self.user
        temp_dict = fs.get_dict_info(path_to_all_users, user_name, 'info')
        user = jp.get_personal_info(temp_dict)
        self.users.append(user)
        return user

    def add_mew_user(self, user_name, password):
        path_to_all_users = fs.get_path_for_users()
        fs.create_user_dir(path_to_all_users, user_name, password)


    def is_duplicate_user_name(self, user_name):
        all_users = fs.get_all_users_name(fs.get_path_for_users())
        return user_name in all_users

    def save_state(self):
        path_for_user = fs.get_path_for_users()
        if self.all_access_right is not None:
            temp_json = jp.get_JSON_from_object(self.all_access_right)
            fs.save_dict_info(path_for_user, None, 'access', temp_json)
        if self.users is not None:
            for user in self.users:
                temp_json = jp.get_JSON_from_object(user)
                fs.save_dict_info(path_for_user, user.name, 'info', temp_json)

    def get_all_users_name(self):
        return fs.get_all_users_name(fs.get_path_for_users())


class JSONManagerTaskStorage(ITaskManagerStorage):
    def __init__(self, current_user):
        self.all_manageres = None
        self.user = current_user

    def get_all_task_managers(self):
        path_to_all_users = fs.get_path_for_users()
        temp_dict = fs.get_dict_info(path_to_all_users, self.user, 'manager')
        result = list()
        for item in temp_dict:
            result.append(jp.get_manager_task(item))
        self.all_manageres = result
        return result

    def get_manager_task_by_tag(self, tag):
        JSONManagerTaskStorage.get_all_task_managers(self)
        for manager in self.all_manageres:
            if manager.tag == tag:
                return tag

    def save_state(self):
        path_to_all_users = fs.get_path_for_users()
        if self.all_manageres is not None:
            temp_dict = jp.get_JSON_from_object(self.all_manageres)
            fs.save_dict_info(path_to_all_users, self.user, 'manager', temp_dict)
