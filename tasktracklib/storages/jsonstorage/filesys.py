import json
import os

from pathlib import Path

TASKS_FILE_NAME = 'tasks.json'
USER_INFO_FILE_NAME = 'info.json'
MANAGER_TASK_FILE_NAME = 'manager.json'
AVAIlABLE_TASK_FILE_NAME = 'available.json'
TASH_ARCHIV_FILE_NAME = 'archiv.json'
ACCESS_TO_TASK_FILE_NAME = 'access.json'

ALL_TYPE_FILE = ['tasks', 'info', 'manager', 'archiv', 'access']
CURRENT_USER_FILE_NAME = '.current'

DEFAULT_GROUP_DICT = [{'name': 'default', 'tasks': []}]
ARCHIV_GROUP_DICT = { 'name': 'arch', 'tasks': []}


def create_user_dir(path_to_dirs_for_user, user_name, user_passw):
    '''
    Creates a directory for the user, with all the necessary files for data storage
    The first parameter takes the path to the directory where all users are stored.
    The second is the user name.
    The third is his password
    Returns the path to this directory

    '''
    dir_path = os.path.join(path_to_dirs_for_user, user_name)
    if os.path.exists(dir_path):
        raise NotImplementedError('Такая папка уже есть')
    os.makedirs(dir_path)
    tasks_path = os.path.join(dir_path, TASKS_FILE_NAME)
    info_path = os.path.join(dir_path, USER_INFO_FILE_NAME)
    manager_path = os.path.join(dir_path, MANAGER_TASK_FILE_NAME)
    archiv_path = os.path.join(dir_path, TASH_ARCHIV_FILE_NAME)
    with open(tasks_path, 'w') as file:
        json.dump(DEFAULT_GROUP_DICT, file, sort_keys=True, indent=4)
    with open(info_path, 'w') as file:
        json.dump({'name': user_name, 'password' : user_passw },
                  file, sort_keys=True, indent=4)
    with open(manager_path, 'w') as file:
        json.dump([], file, sort_keys=True, indent=4)
    with open(archiv_path, 'w') as file:
        json.dump(ARCHIV_GROUP_DICT, file, sort_keys=True, indent=4)
    return dir_path

def create_access_to_tasks(path_to_users_dir):
    '''
    Сreates a file for storing access rights.
    As a parameter, it takes a directory where all information is stored
    about users

        '''
    if not os.path.exists(path_to_users_dir):
        raise FileNotFoundError('Дирректории с пользователямии не существует')
    full_path = os.path.join(path_to_users_dir, ACCESS_TO_TASK_FILE_NAME)
    with open(full_path, 'w') as file:
        json.dump([], file, sort_keys=True, indent=4)
    return full_path

def get_dict_from_file(path_to_file):
    '''
    Returns the dictionary of the contents of the file,
    for further parsing it into the object

    '''
    if not os.path.exists(path_to_file):
        raise FileNotFoundError('Не сущ. пути ' + path_to_file)
    with open(path_to_file, 'r') as file:
        result = json.loads(file.read())
    return result

def save_dict_info(path_to_all_users, user_name, type_file, JSONstr):
    '''
    Saves user information in a file.
    The first parameter is the path to the directory in which all users are stored.
    The second is the user, whose information we want to keep
    The third is the type of information that we want to save ['tasks', 'info', 'manager', 'archiv', 'access'].
    The fourth is the json data line to be saved

    '''
    if not (type_file in ALL_TYPE_FILE):
        raise ValueError('type_file не верный парматр')
    if type_file == 'access':
        full_path = os.path.join(path_to_all_users, AVAIlABLE_TASK_FILE_NAME)
    else:
        full_path = os.path.join(path_to_all_users, user_name,
                                 type_file + '.json')
        if not os.path.exists(full_path):
            raise FileNotFoundError('Нету информации о пользователе')
    with open(full_path, 'w') as file:
        file.write(JSONstr)

def get_dict_info(path_to_all_users, user_name, type_file):
    '''
    Returns information to the user in the form of a dictionary
    (attribute-value) for further parsing.
    The first parameter is the path to the directory in which all users are stored.
    The second is the user whose information we want to receive
    The third is the type of information we want to get
    ['tasks', 'info', 'manager', 'archiv', 'access'].

    '''
    if not (type_file in ALL_TYPE_FILE):
        raise ValueError('type_file не верный парматр')
    full_path = os.path.join(path_to_all_users, user_name, type_file + '.json')
    if not os.path.exists(full_path):
        raise FileNotFoundError('Нету инофрмации о пользователе:' + full_path)
    with open(full_path, 'r') as file:
        result = json.loads(file.read())
    return result

def get_path_for_users(path_for_file_config=None):
    '''The function searches for information about the user directory'''
    path_for_users = os.path.join(os.getcwd(), 'users')
    if not os.path.exists(path_for_users):
        os.mkdir(path_for_users)
        with open(os.path.join(path_for_users,'.current'), 'w'):
            pass
    return path_for_users

def set_current_user(path_to_all_users, user_name):
    '''
    Set flags a user named "user_name" as current (authorizes it)

    '''
    full_path = os.path.join(path_to_all_users, CURRENT_USER_FILE_NAME)
    with open(full_path, 'w') as file:
        file.write(user_name)

def create_available_file(path_to_all_users):
    '''
    Creates a file to store the user's access rights to the tasks of other users

    '''
    full_path = os.path.join(path, AVAIlABLE_TASK_FILE_NAME)
    with open(full_path, 'w') as file:
        json.dump(list(), file, sort_keys=True, indent=4)

def get_access_file(path_to_all_users):
    '''
    Returns the dictionary with all user access rights to the tasks of other users

    '''
    full_path = os.path.join(path_to_all_users, AVAIlABLE_TASK_FILE_NAME)
    if not os.path.exists(full_path):
        create_available_file(path_to_all_users)
    with open(full_path) as file:
        result = json.loads(file.read())
    return result


def get_all_users_name(path_to_dir):
    '''
    Returns the names of all users whose directories were created
    (registered users)

    '''
    all_user = [file.split('.')[0] for file in os.listdir(path=path_to_dir) if
                not ('.' in file)]
    return all_user
