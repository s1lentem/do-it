'''The module stores the interfaces that need to be implemented,
That the object of type DataStorageManager normally functioned'''


class ITaskStorage:
    '''Task Store Interface'''

    def get_task_by_tag(self, tag, user=None):
        '''Getting (tasks, task groups) by tag'''
        raise NotImplementedError()

    def get_all_groups(self):
        '''Getting all groups with tasks, for a particular user'''
        raise NotImplementedError()

    def get_group(self, group_name):
        '''Obtaining a specific task group from the current user'''
        raise NotImplementedError()

    def get_all_tasks(self):
        '''Getting all the tasks of your current user'''
        raise NotImplementedError()

    def tag_is_not_busy(self, tag):
        '''Checking the validity of the tag,
        that is, whether the tag is busy with another task

        '''
        raise NotImplementedError()

    def get_archived_tasks(self):
        '''Getting the task archive'''
        raise NotImplementedError()

    def save_state(self):
        '''The method for saving the status of the task store

        When you call any other methods of this class, the user receives
        Access to information that could well change.
        This method should gently save all those changes to the data,
        which the user made

        '''
        raise NotImplementedError()

class IUserStorage:
    '''Information storage interface with user

    Here, too, information about the access rights of various users
    to various tasks. It is assumed that the user has defaulted on a full
    access to tasks, and access can only be to a stranger

    '''
    def get_all_access_manager(self):
        '''Returns a list of AccessRights objects for all users'''
        raise NotImplementedError()

    def get_access_right_for_master_with_tag(self, tag, master=None):
        '''Returns a list of AccessRights objects in which the user
        master gave access to the task by tag tag
        None means the current user

        '''
        raise NotImplementedError()

    def get_users_with_access_to(self, tag, task_master=None):
        '''Returns a list of objects of the user type in which the user
        the wizard gave access to the task with the tag tag
        None means that the current user

        '''
        raise NotImplementedError()

    def remove_access_right(self, task_tag, user=None):
        '''Method for removing access rights for specific tasks
        user = None means removing all access rights for a task with a tag tag,
        otherwise only for a specific user

        '''
        raise NotImplementedError()

    def get_tasks_are_available_for(self, user_name):
        '''Returns a list of objects of type AccessRights, for which user_name
        has access to any tasks
        '''
        raise NotImplementedError()

    def get_user(self, user_name):
        '''Returns a User object by its name (user_name)'''
        raise NotImplementedError()

    def give_access_right_to(self, access_right_info):
        '''Gives access to the backside, all information is stored in access_right_info'''
        raise NotImplementedError()

    def is_duplicate_user_name(self, user_name):
        '''Return true if the name is already taken'''
        raise NotImplementedError()

    def add_new_user(self, user_name):
        '''Adds a new user to the store'''
        raise NotImplementedError()

    def save_state(self):
        '''The method for storing the state of the user store

        When you call any other methods of this class, the user receives
        access to information, which may well change.
        This method should store all those changes in the data,
        which the user made
        '''
        raise NotImplementedError()

    def get_all_users_name(self):
        raise NotImplementedError()

class ITaskManagerStorage:
    '''Interface for storing information about Task Managers'''

    def get_all_task_managers(self):
        '''Returns a list of objects of type Manager Task for a particular user'''
        raise NotImplementedError()

    def get_manager_task_by_tag(self, tag):
        '''Returns objects of type ManagerTask to a particular user
        with tag = tag (method parameter)
        '''
        raise NotImplementedError()

    def save_state(self):
        '''The method for storing the status of the task managers store

        When you call any other methods of this class, the user receives
        Access to information that could well change.
        This method should allow saving all those changes in the data,
        which the user made
        '''
        raise NotImplementedError()
