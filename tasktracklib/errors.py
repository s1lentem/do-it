class DuplicateTagError(Exception):
    '''The exception that occurs when you try to add a task to an existing task
     tag'''
    def __init__(self, message):
        self.message = message
    pass

class DuplicateUserNameError(Exception):
    def __init__(self, message):
        self.message = message
    pass

class NotSuitableDictionaryError(Exception):
    '''The exception that is generated when a predecessor does not speak the expected
    The dictionary does not contain enough information to restore the object'''
    def __init__(self, message):
        self.message = message
    pass

class NonExistentArgumentError(Exception):
    '''An exception is thrown when you try to pass a nonexistent argument'''
    def __init__(self, message):
        self.message = message
    pass

class InvalidNumberOfArgumentsError(Exception):
    '''An exception is generated when an incorrect number of parameters is transmitted'''
    def __init__(self, message):
        self.message = message
    pass

class IncorrectDateError(Exception):
    '''Exception generated on an incorrect date'''
    def __init__(self, message):
        self.message = message

class EndTaskGeneration(Exception):
    '''Generation at the end of the period for the formation of regular tasks'''
    def __init__(self, manager):
        self.manager = manager

class AccessToYourself(Exception):
    '''When you try to give yourself access'''
    def __init__(self, message):
        self.message = message


class TaskNotFoundError(Exception):
    '''If the task was not found'''
    def __init__(self, message):
        self.message = message
