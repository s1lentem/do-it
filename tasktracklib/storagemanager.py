import datetime as dt

from collections import namedtuple

import tasktracklib.storages.storage_interface as istorage
import tasktracklib.errors as errors

from tasktracklib.components.taskcomp import (DependenceType, AccessType,
                                              StatusType, Priority)
from tasktracklib.components.user import AccessRights
from tasktracklib.dateparse import checking_notification_date, get_datetime
from tasktracklib.logger import get_lib_log

__all__ = ['DataStorageManager']

ALL_FIELD_NAME = ['tag', 'dateend', 'datebegin', 'description', 'priority',
                  'status']


class DataStorageManager:
    '''A class that implements the entire logic of interaction between tasks and users.

    The constructor takes three objects that implement ITaskStorage,
    IUserStorage and ITaskManagerStorage respectively and the fourth parameter
    user name in string format.

    '''
    def __init__(self, task_storage, user_storage, task_manager_storage,
                 current_user, path_to_file_for_log=None, level_log=None):
        self.task_storage = task_storage
        self.user_storage = user_storage
        self.task_manager_storage = task_manager_storage
        self.current_user = current_user
        self.log = get_lib_log(path_to_file_for_log, level_log)


    def send_notification(self, users_names, message):
        '''user_name - the name of the users who need to leave a message
        message - the message itself

        '''
        for user_name in users_names:
            user = self.user_storage.get_user(user_name)
            user.leave_message(message)

            self.log.info('User ' + self.current_user + ' leave message for ' +
                         user_name)


    def add_task(self, task, group_name):
        '''Method for adding a task to the current user

        task is an object of type Task, which we want to add
        group_name - the name of the group to which you want to add the task
        '''
        if task.timing.end < dt.datetime.now(task.timing.end.tzinfo):
            self.log.error('End date (%s) before current date (%s)',
                           str(dt.datetime.now()), str(task.timing.end))

            raise errors.IncorrectDateError('Invalid task completion date' +
                                            str(task.timing.end))
        begin = task.timing.begin
        end = task.timing.end
        date_begin = dt.datetime(begin.year, begin.month, begin.day,
                                 hour=begin.hour, minute=begin.minute)
        date_end = dt.datetime(end.year, end.month, end.day,
                                 hour=end.hour, minute=end.minute)
        if date_end < date_begin:
            self.log.error('End date ({}) before begin date ({})'.format(str(task.timing.end),
                                                                         str(task.timing.begin)))

            raise errors.IncorrectDateError('Date end: ' +
                                            str(task.timing.end) +
                                            ' before begin date ' +
                                            str(task.timing.begin))
        tasks_group = self.task_storage.get_group(group_name)
        if self.task_storage.tag_is_not_busy(task.tag):
            tasks_group.add_task(task)
            self.task_storage.save_state()

            self.log.info('User %s added task with tag %s ', self.current_user, task.tag)
        else:
            self.log.error('User %s could not add the task,since the tag (%s) has already been accepted',
                           self.current_user, task.tag)

            raise errors.DuplicateTagError()


    def get_tasks(self, type_tasks=None, param=None):
        '''Method for obtaining objects of type Task

        If group_name = None, returns absolutely all tasks available for
        this user
        If group_name = arch, returns the tasks stored in the archive
        Otherwise, returns tasks in a specific group

        '''
        if type_tasks is None:
            self.log.info('User ' + self.current_user + ' has requested all tasks')
            return self.task_storage.get_all_tasks()
        else:
            if type_tasks == 'arch':
                self.log.info('User %s has requested archieved tasks', self.current_user)

                return self.task_storage.get_archived_tasks()
            elif type_tasks == 'group':
                self.log.info('User %s has requested tasks from %s',
                              self.current_user, param)

                return self.task_storage.get_group(param).tasks
            elif type_tasks == 'status':

                return [task for task in self.task_storage.get_all_tasks() if
                        task.status.state is param]

            elif type_tasks == 'priority':

                return [task for task in self.task_storage.get_all_tasks() if
                        task.status.priority is param]


    def get_tasks_are_available_for(self, user=None):
        '''The method returns the tasks that the user had access to (except for his own)
         user is the string that stores the user name

        '''
        if user is None:
            user = self.current_user
        all_access_for_user = self.user_storage.get_tasks_are_available_for(user)
        all_tasks = list()
        for access_right in all_access_for_user:
            task = self.task_storage.get_task_by_tag(access_right.tag, access_right.master)[0]
            all_tasks.append((task, access_right.master))

        self.log.info('User %s has requested all tasks to which he had access',
                      self.change_task)

        return all_tasks


    def check_access_right_for_read(self, master, tag):
        '''Checks if the current user has access to the task with the tag
        owned by the master

        All parameters have a row type

        '''
        user = self.user_storage.get_current_user_name()
        access_rights = self.user_storage.get_access_right_for_master_with_tag(tag, master)
        for item in access_rights:
            if item.user == user:
                return True
        return False


    def check_deadline_for_each_task(self):
        '''The method returns all failed tasks of the current user'''
        all_tasks = self.task_storage.get_all_tasks()
        overdue_task = list()
        current_date = dt.datetime.now()

        for task in all_tasks:
            if (task.timing.end < current_date and
                task.status.state is not StatusType.completed):
                task.status.state = StatusType.failed
                overdue_task.append(task)
        self.task_storage.save_state()

        self.log.info('all tasks have been checked for deadline in %s',
                      self.current_user)

        return overdue_task

    def checking_date_for_notification(self):
        '''The method of checking for the need to inform at this time
        user about the approach of the deadline of some tasks.
        Returns a list of these tasks

        '''
        all_notifications = list()
        all_tasks = self.task_storage.get_all_tasks()
        for task in all_tasks:
            if task.timing.end is None or task.types_notification is None:
                continue
            notice_information = checking_notification_date(task.timing.end,
                                                            task.types_notification)
            notify = notice_information[0]
            if notice_information[1] == '':
                task.types_notification = None
            else:
                task.types_notification = notice_information[1]
            if notify:
                all_notifications.append(task)
        self.task_storage.save_state()

        self.log.info('all tasks have been checked for notifications in %s',
                      self.current_user)

        return all_notifications

    def put_task_in_archive(self, tag):
        '''Puts the task with the tag into an archive'''
        task_info = self.task_storage.get_task_by_tag(tag)
        archiv = self.task_storage.get_archived_tasks()
        task_info[1].remove_task_by_tag(tag)
        archiv.add_task(task_info[0])
        self.task_storage.save_state()
        access_rights = self.user_storage.get_users_with_access_to(tag)
        observers = [user.name for user in access_rights]
        self.send_notification(observers,
                               'user {} put in arrhiv {}'.format(self.current_user, tag))
        self.user_storage.remove_access_right(tag)
        self.user_storage.save_state()

        self.log.info('User %s put task(%s) in archiv', self.current_user, tag)

        all_tasks = self.task_storage.get_all_tasks()
        for task in all_tasks:
            if (task.dependencies is not None and
                task.dependencies.task_tag == task_info[0].tag):
                task.dependencies = None
        self.task_storage.save_state()

    def update_tag_in_access_right(self, old_tag, new_tag):
        '''Permissions change the old old_tag tag to a new new_tag,
        for to adjust

        '''
        all_old_access_right = self.user_storage.get_access_right_for_master_with_tag(old_tag)
        for old_access_right in all_old_access_right:
            old_access_right.tag = new_tag
        self.user_storage.save_state()

        self.log.info('There is a need to update the tags in the access rights: %s to %s',
                      old_tag, new_tag)

    def check_dependece_for_change_status(self, task, value):
        '''A method that allows you to determine whether an object of the object
        is a task, change your status to the value of the row type

        '''
        if isinstance(value, str):
            value = StatusType[value]
        if task.dependencies is None:
            return True
        if task.dependencies.type is DependenceType.parallel:
            if task.dependencies.master is None:
                master_task = self.task_storage.get_task_by_tag(task.dependencies.task_tag)[0]
            else:
                master_task = self.task_storage.get_task_by_tag(task.dependencies.task_tag,
                                                                task.dependencies.master)[0]
            return not (value is StatusType.completed and
                        master_task.status.state is not StatusType.completed)
        if task.dependencies.type is DependenceType.stoped:
            if task.dependencies.master is None:
                master_task = self.task_storage.get_task_by_tag(task.dependencies.task_tag)[0]
            else:
                master_task = self.task_storage.get_task_by_tag(task.dependencies.task_tag,
                                                                task.dependencies.master)[0]
            return master_task.status.state is StatusType.completed
        return True

    def link_task_with(self, user, master_tag, sub_tag, link_type):
        '''Allows you to associate a task with the mastet_tag (type: string)
        the current user with the sub_tag (type: string) task of the user
        user (type: string) by one of the link_type link types (type: string) or
        completely reset these links by passing as "link_type" reset "(when
        this user and sub_tag are optional)

        '''
        master_task = self.task_storage.get_task_by_tag(master_tag)[0]
        if link_type == 'reset':
            master_task.reset_link()
        elif user is None:
            master_task.link_to(sub_tag, link_type)
        else:
            if check_access_right_for_read(user, sub_tag):
                master_task.link_to(sub_tag)

                self.log.info('User %s binds the task (%s) to user %s task %s as a %s',
                              self.current_user, master_task, user, tag, link_type)

        self.task_storage.save_state()

    def break_link_with_task(self, tag):
        task = self.task_storage.get_task_by_tag(tag)[0]
        task.reset_link()
        self.task_storage.save_state()

    def edit_field_task(self, task, value, field_name):
        '''Allows a Task object to change the field_name attribute to value'''
        if not (field_name in ALL_FIELD_NAME):
            raise NotImplementedError('Invalid params')

        if field_name == 'tag':
            self.update_tag_in_access_right(task.tag, value)
            task.tag = value
        elif field_name == 'dateend':
            task.timing.end = get_datetime(value)
        elif field_name == 'datebegin':
            task.timing.begin = get_datetime(value)
        elif field_name == 'description':
            task.description = value
        elif field_name == 'priority':
            task.status.priority = Priority[value]
        elif field_name == 'status':
            if self.check_dependece_for_change_status(task, value):
                task.status.state = StatusType[value]
        task.timing.change = dt.datetime.now()

    def change_task(self, tag, field, value, user=None):
        '''Allows you to change the field attribute of a task with a tag tag to
        a value for the user user

        '''
        user_name = self.current_user
        task = self.task_storage.get_task_by_tag(tag)
        if task[0] is None:
            raise errors.TaskNotFoundError(tag)
        self.edit_field_task(task[0], value, field)
        users = self.user_storage.get_users_with_access_to(task[0].tag)
        self.send_notification([user.name for user in users ],
                               'user ' + user_name + ' change task')
        self.task_storage.save_state()

        self.log.info('User ' + self.current_user + ' change task: ' + tag +
                      ' field: ' + field)


    def editing_someone_else_task(self, master, tag, field, value):
        access = self.user_storage.get_access_right_for_master_with_tag(tag, master)

        if self.current_user in [item.user for item in access if
                                 item.type.name == 'write']:
            task_info = self.task_storage.get_task_by_tag(tag, master)

            self.edit_field_task(task_info[0], value, field)
            self.send_notification([master], 'User ' + self.current_user +
                                   'change task: ' + tag)
            self.task_storage.save_state()
            # print('noo')
            self.log.info('User ' + self.current_user + ' change: ' + tag +
                          ' field: ' + field + ' user task ' + master)


    def get_all_messages_user(self, user_name=None, delete_all_messages=True):
        '''Returns all notifications for a user named user_name

        delete_all_messagesSelect to delete all messages after
        their return

        '''
        user = self.user_storage.get_user(user_name)
        messages = user.messages
        if delete_all_messages:
            user.messages = list()
        self.user_storage.save_state()

        self.log.info('All notifications have been requested for the user: ' +
                      user.name + ' with parameter delete_all_messages - ' +
                      str(delete_all_messages))

        return messages

    def register_user(self, user_name, password):
        '''Registers a user named user_name and password in the system'''

        if self.user_storage.is_duplicate_user_name(user_name):
            self.log.error('Attempt to create a user with an already occupied '
                          'name: ' + user_name)

            raise errors.DuplicateUserNameError('Имя ' +  user_name +
                                                    ' уже занято')
        self.user_storage.add_new_user(user_name, password)

        self.log.info('A new user has been created ' + user_name)


    def add_manager_task(self, manager):
        '''Adds an object of type Task Manager for the current user'''

        all_manageres = self.task_manager_storage.get_all_task_managers()
        if self.task_storage.tag_is_not_busy(manager.tag):
            all_manageres.append(manager)
            self.task_manager_storage.save_state()

            self.log.info('User ' + self.current_user + ' added manager task')
        else:
            self.log.error('The user ' + self.current_user + ' tried to add a '
                           'task manager with an already occupied tag')

            raise errors.DuplicateTagError()

    def check_tasks_from_managers(self):
        '''Checks the need to generate tasks for all TaskManager
         current user

        '''
        all_manageres = self.task_manager_storage.get_all_task_managers()
        for manager in all_manageres:
            task = self.task_storage.get_task_by_tag(manager.tag)

            if task is None:
                if not manager.period_is_complete():
                    self.add_task(manager.create_task(), 'default')
                else:
                    raise errors.EndTaskGeneration(manager)
        self.task_manager_storage.save_state()

        self.log.info('Verification of the generation of tasks by scenarios '
                      'for' + self.current_user)


    def give_access_to(self, tag, user_name, access_type):
        '''Grants access_type access (AccessType) to a task with the tag tag
         current user user_name (str)

        '''
        if self.current_user == user_name:
            raise errors.AccessToYourself(user_name)
        access_right_info = AccessRights(self.current_user, user_name, tag, access_type)
        self.user_storage.give_access_right_to(access_right_info)
        self.send_notification([user_name], 'User ' +
                               str(self.current_user) + ' give access to ' +
                               tag)
        self.user_storage.save_state()

        self.log.info('User ' + str(self.current_user) + ' give access rights to ' +
                      user_name)

    def get_away_access_from(self, tag, user_name):
        '''Retrieves the access rights to the task with the tag of the current user
         user_name

        '''
        all_old_access_right = self.user_storage.get_all_access_manager()
        num = 0
        data_was_found = False
        for item in all_old_access_right:
            if item.tag == tag and item.user == user_name:
                data_was_found = True
                break
            num+=1
        if not data_was_found:
            self.log.error('By picking off access rights from a user who did '
                           'not have these rights ')

            raise NotImplementedError()
        all_old_access_right.pop(num)
        self.user_storage.save_state()

        self.send_notification([user_name], 'User ' + self.current_user +
                               ' remove access right to ' + tag)

        self.log.info('The user ' + self.current_user + ' has removed the '
                      'access rights of ' + user_name)


    def reset_archiv(self):
        '''Clears the current user's archive'''
        archiv = self.task_storage.get_archived_tasks()
        archiv.clear()
        self.task_storage.save_state()

        self.log.info('User ' + str(self.current_user) + ' reset archiv')


    def get_all_users_name(self):
        '''Returns the names of all registered users'''
        return self.user_storage.get_all_users_name()


    def get_user(self, user_name):
        ''''Returns an object of type 'user' with the name user_name'''
        return self.user_storage.get_user(user_name)


    def get_task_by_tag(self, tag):
        '''Returns an object of type "task" with the tag "tag"'''
        task_info = self.task_storage.get_task_by_tag(tag)
        return task_info[0] if task_info is not None else None


    def get_project_from(self, root, all_tasks=None, project=None):
        '''Forms a project (task tree) based on the root task (param "root").
        Calling only with the first argument, the rest are required for
        recursive calls


        '''
        if all_tasks is None:
            all_tasks = (self.task_storage.get_all_tasks() +
                         [task[0] for task in self.get_tasks_are_available_for()])

        if project is None:
            project = namedtuple('task', ['task_info', 'child_tasks'])
            project.task_info = root
            project.child_tasks = list()

        for task in all_tasks:
            if (task.dependencies is not None and
                task.dependencies.task_tag == root.tag):

                branch_project = namedtuple('task', ['task_info', 'child_tasks'])
                branch_project.task_info = task
                branch_project.child_tasks = list()
                project.child_tasks.append(branch_project)


        all_tasks = list(set(all_tasks) -
                         set([branch.task_info for branch in project.child_tasks]))
        for task in project.child_tasks:
            self.get_project_from(task.task_info, all_tasks, task)
        return project


    def complete_task(self, tag):
        '''Marks the task as "complete",
        Such a task can not be unsuccessful and remains where it was


        '''

        task = self.task_storage.get_task_by_tag(tag)[0]
        if self.check_dependece_for_change_status(task, StatusType.completed):
            task.status.state = StatusType.completed
        self.task_storage.save_state()

        observers = self.user_storage.get_users_with_access_to(tag)
        self.send_notification([observer.name for observer in observers],
                               self.current_user + ' complete task ' +
                               tag)
        self.user_storage.save_state()

        tasks = self.task_storage.get_all_tasks()
        for task in tasks:
            if (task.dependencies is not None and
                task.dependencies.task_tag == tag and
                task.dependencies.type is DependenceType.subtask):
                if self.check_dependece_for_change_status(task, StatusType.completed):
                    task.status.state = StatusType.completed
        self.task_storage.save_state()


    def remove_task(self, tag):
        '''Completely removes the task'''

        task_info = self.task_storage.get_task_by_tag(tag)
        group = task_info[1]
        task = task_info[0]
        group.remove_task_by_tag(tag)
        self.task_storage.save_state()

        observers = self.user_storage.get_users_with_access_to(tag)
        self.send_notification([user.name for user in observers],
                               self.current_user + ' remove task ' + tag)
        self.user_storage.remove_access_right(tag)
        self.user_storage.save_state()

        all_tasks = self.task_storage.get_all_tasks()
        for task in all_tasks:
            if (task.dependencies is not None and
                task.dependencies.task_tag == tag):
                task.dependeceies = None

    def remove_manager_task(self, tag):
        '''Completely removes the task'''
        all_manager = self.task_manager_storage.get_all_task_managers()
        index = 0
        is_removed = False
        for manager in all_manager:
            if manager.tag == tag:
                is_removed = True
                break
            index += 1
        if is_removed:
            all_manager.pop(index)
        self.task_manager_storage.save_state()
