'''Module designed to work with dates
-------------------------------------------------- -----------------------------
The get_datetime method turns the list from the date and time (only dates are possible)
into a datetime object

The get_next_date method receives from the current date the following, in agreement in the period,
transmitted by the second parameter

The checking_notification_date method returns a tuple of two values, necessary
for task notification
'''

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

def get_datetime(args):
    '''As args, it takes a list of two or fewer lines
    The first one is the date: year, month, day (or vice versa)
    The second is the time (hour, minute)
    On output gets object of type datetime
    '''
    if len(args) == 0:
        return None
    if len(args) == 2:
        date = args[0].replace('.', ' ').replace(':', ' ').replace('-', ' ').split(' ')
        time = args[1].split(':')
    else:
        date = args.replace('.', ' ').replace(':', ' ').replace('-', ' ').split(' ')
        time = [0,0]
    date = [int(item) for item in date]
    time = [int(item) for item in time]
    if date[0] < date[2]:
        date = date[::-1]
    return datetime(date[0], date[1], date[2], time[0], time[1])

def get_next_date(current_date, period):
    '''Gets the next date for the current date based on the period type
    current_date must be an object of type datetime
    (day, month, week),
    separated by a space, a characterizing interval between dates

    '''
    type_period = period.split(' ')[0]
    dimension = int(period.split(' ')[1])
    if type_period == 'day':
        new_date = current_date + timedelta(days=dimension)
    elif type_period == 'month':
        new_date = current_date + relativedelta(months=+6)
    elif type_period == 'week':
        new_date = current_date + timedelta(days=7*dimension)
    else:
        raise NotImplementedError('Не верный тип периода')
    return new_date

def checking_notification_date(date_end, types_notifications):
    '''The method required to check the current date for what is currently
    it is necessary to take any action based on the information about the periods

    date_end is the date that the current date is compared with types_notifications

    types_notifications is a string from the first number and one of the following. words
    (days, hours, week), separated by spaces, such lines can be many,
    so they also form a single line, separated by the symbol '|'.

    The method is necessary to check whether the user needs to be notified
    on the deadline of the task

    At the output, the tuple from bool (true - needs to be notified, false - not needed) and
    line types_notifications without the substring that was used
    '''
    all_types = types_notifications.split('|')
    is_notify = False
    num_notify = 0
    current_date = datetime.now()
    if all_types[0] == '':
        return (False, types_notifications)
    for type_notification in all_types:
        type_info = type_notification.split(' ')
        if type_info[1] == 'days':
            delta = timedelta(days=int(type_info[0]))
        elif type_info[1] == 'hours':
            delta = timedelta(hours=int(type_info[0]))
        elif type_info[1] == 'week':
            delta = timedelta(weeks=int(type_info[0]))
        if date_end - delta <= current_date:
            is_notify = True
            break
        num_notify += 1
    if is_notify:
        all_types.pop(num_notify)
        types_notifications = '|'.join(all_types)
        return (True, types_notifications)
    return (False, types_notifications)
