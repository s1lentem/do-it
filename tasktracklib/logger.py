import logging

__all__ = ['get_lib_log']

FORMAT = u'%(levelname)-8s [%(asctime)s]  %(message)s'
FILENAME = 'LIBLOG.log'


def get_level_logger_from_str(level_str):
    if level_str == 'DEBUG':
        return logging.DEBUG
    elif level_str == 'INFO':
        return logging.INFO
    elif level_str == 'WARNING':
        return logging.WARNING
    elif level_str == 'ERROR':
        return logging.ERROR
    elif level_str == 'NOTSET' or level_str is None:
        return logging.NOTSET

def get_lib_log(path_to_file_for_log, level):
    '''Return logger for tasktracklib
    path_to_file_for_log - the path to the file that stores logs,
    if None, writes to the program directory

    level - defines logging levels (DEBUG, INFO, WARNING, ERROR)

    '''
    if path_to_file_for_log is None:
        path_to_file_for_log = FILENAME
    level = get_level_logger_from_str(level)
    logging.basicConfig(filename=path_to_file_for_log, format=FORMAT,
                        level=level)
    return logging.getLogger(name='lib')
